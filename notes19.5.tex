\section{Lecture: Thursday, November 15th}
Note taker: Gianluca Gross
\label{sec:LC20}

\subsection{Universal Finiteness Problem}
\label{sec:LC20:UniversalFiniteness}

In the last lecture, we discussed the finiteness problem for Datalog. As a quick reminder, the finiteness problem is as follows: Given a program and a particular edb, will a particular tuple have an infinite or finite number of derivations? We discussed an algorithm for solving this problem, thus showing that this property is computable in finite time. In fact, we can solve the finiteness problem for stratified Datalog$^{\neg}$ by simply applying that same algorithm stratum-by-stratum.

Now we will discuss the Universal finiteness problem for Datalog, which is discussed in the paper by Mumick and Shmueli
\href{https://dl.acm.org/citation.cfm?doid=182591.182612}{here}.
The universal finiteness problem is defined as follows: Given a Datalog program, will all tuples have finite multiplicity on any edb instance? Note that this is a static property of a Datalog program because we are asked to evaluate the finiteness property for any edb. As an example, the transitive closure program does not possess the universal finiteness property because any edb which contains a cycle will cause some tuples to have an infinite number of derivations. We will show that the universal finiteness problem for Datalog is undecidable, but we will first provide a quick review of reductions.

\subsection{Review of Reductions}
\label{sec:LC20:ReductionReview}

In simple terms, a reduction is an algorithm for transforming one problem to another. In particular, suppose we are given two problems $P_1$ and $P_2$. If $P_1$ is reducible to $P_2$ (i.e. $P_1 \leq_{reduction} P_2$), then we know that $P_2$ is at ``at least as hard'' as $P_1$ because finding a way to solve $P_2$ implies that we can retroactively solve $P_1$ as well.

Reductions are extremely useful for demonstrating the computational hardness/complexity of different problems. For example suppose $P_1$ is an undecidable problem, and there exists a computable algorithm for reducing $P_1$ to $P_2$. This implies that $P_2$ is also undecidable (because if $P_2$ was decidable, then we could retroactively decide $P_1$, a contradiction of the fact that $P_1$ is undecidable). To show NP-hardness, reductions must be computable in polynomial time. To show PTIME-hardness, reductions must be computable in log space.

Now we will demonstrate that the universal finiteness problem for stratified Datalog$^{\neg}$ is undecidable by showing a reduction from the Datalog program equivalence problem (which is undecidable).

\subsection{Undecidability of Universal Finitess Problem}
\label{sec:LC20:UnivFinUndec}

We will reduce Datalog program equivalence to the universal finiteness problem for stratified Datalog$^{\neg}$. Suppose we are given two Datalog programs $P_1$ and $P_2$, which have corresponding output idbs $Q_1$ and $Q_2$. We construct a new Datalog program $P$ (with output idb $Q$) which is simply a concatenation of all the rules in $P_1$ and $P_2$ as well as the following three additional rules:
\begin{align*}
Q\bar{x} &\coloneq Q_1\bar{x}, \neg Q_2\bar{x} \\
Q\bar{x} &\coloneq Q_2\bar{x}, \neg Q_1\bar{x} \\
Q\bar{x} &\coloneq Q\bar{x}
\end{align*}
Now we will show that $P_1$ and $P_2$ are equivalent if and only if $P$ has the universal finiteness property.

If $P_1$ and $P_2$ are equivalent, then we know that on any input edb, $Q_1$ and $Q_2$ will contain the same exact tuples. However, looking at the rules for deriving tuples in $Q$, we can see that $Q$ will only contain tuples which are in $Q_1 - Q_2$ or $Q_2 - Q_1$. Since $Q_1$ and $Q_2$ will always contain the same tuples, we can thus conclude that $Q$ will always be empty, implying that $P$ has the universal finiteness property.

If $P_1$ and $P_2$ are not equivalent, then there exists some input edb such that $Q_1$ and $Q_2$ are not equal. This means that there is some tuple which is in $Q_1 - Q_2$ or $Q_2 - Q_1$, and so $Q$ will be nonempty. However, due to the rule $Q\bar{x} \coloneq Q\bar{x}$, we can see that any tuple in $Q$ will have and infinite number of derivations. This means that $P$ does not possess the universal finiteness property in this case.

Thus, we have successfully reduced Datalog program equivalence to the universal finiteness problem for stratified Datalog$^{\neg}$ (because we used negation in our reduction). This shows that the universal finiteness problem is undecidable for stratified Datalog$^{\neg}$.

\subsection{Context Free Grammars and Datalog Programs}
\label{sec:LC20:CFGDatalog}

Now we will explore the relationship between context free grammars (CFGs) and Datalog programs.

Given a CFG $G$, we can decide if $L(G)$ is finite. However, given two CFGs $G_1$ and $G_2$ over the same terminals, determining if $L(G_1) \subseteq L(G_2)$ is undecidable.

We would like to find a general procedure for mapping an arbitrary CFG $G$ to a Datalog program $P_G$ such that the following holds: Given two CFGs $G_1$ and $G_2$, $L(G_1) \subseteq L(G_2)$ if and only if $P_{G_1} \sqsubseteq P_{G_2}$ (i.e. for all edbs $E$, $P_{G_1}(E) \subseteq P_{G_2}(E)$).

Since CFGs are used to generate strings, it is clear that the ordering of terminals and non-terminals in the rules is very important. However, the atoms in Datalog rules do not necessarily adhere to a specific order. As such, we need our mapping procedure to somehow enforce the ordering of the original CFG in the resulting Datalog program.

We will demonstrate the general idea of this procedure through an example. Consider the following CFG with terminals $a$ and $b$, and non-terminals $S$ and $T$:
\begin{align*}
r1: \quad S &\rightarrow aT \\
r2: \quad T &\rightarrow aTb \\
r3: \quad T &\rightarrow b
\end{align*}
This CFG generates strings of the form $a^nb^n$ for $n \geq 1$. An example parse tree for the string $aabb$ is:

\begin{center}
\begin{tikzpicture}[grow'=down,scale=0.75]
\Tree [.$S$
[ . \node(n1){$T$};
	[ . \node(n6){$b$};]
	[ . \node(n4){$T$};		
		[ . \node(n7){$b$};]
	]
	[ . \node(n5){$a$};]
]
[ . \node(n3){$a$};]
]
]
\end{tikzpicture}
\end{center}

Now we will convert this CFG to a Datalog program. We use one edb $R$ to represent the terminals of the CFG, as well as idbs $S$ and $T$ to represent the non-terminals in the CFG. Note that $a$ and $b$ are constants in this program which correspond to the terminals in the CFG.
\begin{align*}
r1: \quad Sxy &\coloneq Rxaz, Tzy \\
r2: \quad Txy &\coloneq Rxaz, Tzu, Ruby \\
r3: \quad Txy &\coloneq Rxby
\end{align*}

As we can see, the rules of this Datalog program correspond directly to the rules in the original CFG. Note that $R$ is a ternary relation while $S$ and $T$ are binary relations. The middle value of each tuple in $R$ is a terminal in the original CFG, but all other variables in this program are are essentially used to enforce an ordering between the atoms in the Datalog rules. For example, in $r1$, the last value $z$ of the tuple in $R$ must correspond to the first value in the tuple of $T$. This is the main mechanism for enforcing the ordering that we discussed earlier.

When constructing the input database, we can simply invent arbitrary constants that will be used to enforce this ordering. For example, here is an example of a derivation tree for this Datalog program that corresponds to the parse tree of the original CFG which we showed earlier, where the $c_i$ values are such arbitrary constants:

\begin{center}
\begin{tikzpicture}[grow'=down,scale=0.75]
\Tree [.$Sc_1c_2$
[ . \node(n1){$Tc_3c_2$};
	[ . \node(n6){$Rc_5bc_2$};]
	[ . \node(n4){$Tc_4c_5$};		
		[ . \node(n7){$Rc_4bc_5$};]
	]
	[ . \node(n5){$Rc_3ac_4$};]
]
[ . \node(n3){$Rc_1ac_3$};]
]
]
\end{tikzpicture}
\end{center}

From this we can see that this procedure yields a direct correspondence between the parse trees of a CFG $G$ and the derivation trees of the corresponding Datalog programs $P_G$.

Now we would just like to show that this conversion procedure has the following property: Given two CFGs $G_1$ and $G_2$, $L(G_1) \subseteq L(G_2)$ if and only if $P_{G_1} \sqsubseteq P_{G_2}$. This essentially follows directly from the correspondence between parse trees and derivation trees.

Suppose $L(G_1) \subseteq L(G_2)$. Now consider any string $s \in L(G_1)$. This means that there is a parse tree which derives $s$ for $G_1$, and a corresponding derivation tree for $P_{G_1}$. Since $L(G_1) \subseteq L(G_2)$, we know that $s \in L(G_2)$, so there is a parse tree which derives $s$ for $G_2$ as well as a corresponding derivation tree for $P_{G_2}$. Thus, any tuple which is derived in $P_{G_1}$ will also be derived in $P_{G_2}$, implying that $P_{G_1} \sqsubseteq P_{G_2}$.

The opposite direction of the proof follows an analogous line of reasoning.

Thus, we have successfully reduced the problem of deciding if $L(G_1) \subseteq L(G_2)$ to deciding if $P_{G_1} \sqsubseteq P_{G_2}$. Since determining if $L(G_1) \subseteq L(G_2)$ is undecidable, this implies that determining if $P_1 \sqsubseteq P_2$ for two Datalog programs $P_1$ and $P_2$ is also undecidable.