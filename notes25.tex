\section{Lecture: Thursday, November 6th}
Note taker: Richard Zhang
\label{sec:LC25}

In this lecture we will talk about another framework for distributed Datalog called WebDamLog.

Web comes from the fact that this language is used between devices communicating using the web. The first question would be why this is called a "dam". The metaphor here is that since there is a large amount of data on the web, we need some way to filter it. The Datalog-like language presented here is supposed to help us interpret this large amount of information.

This language is more complicated than the languages we discussed last time, such as DBLog or NDLog. However, similar to NDLog, relations have two parts: actual name and a location.

\paragraph{Example of facts.}
This relationship is supposed to encode the info that allows us to automatically send a “Happy Birthday” message to a friend.

\begin{center}
$birthday@myIphone(“Alice”, sendmail, inria.fr, 08/08)$
$birthday@myIphone(“Bob”, sms, BobIphone, 01/12)$
\end{center}

Here, birthday is the name of my relation, while myIphone is the site at which this rule is installed (this information about birthdays is being stored at my phone. Note that sendmail and sms are relations in themselves - we can store the names of relations in our EDB predicates.

\paragraph{Example of rules.}

The paper uses \$ to indicate variables that should be generalized. Here I will use ? since the dollar sign is inconvenient in Latex.

Assuming we have a rule similar to the one above:

\begin{center}
$?mode@?peer(?name, “HappyBirthday”)$ :- $today@myIphone(?d), birthday@myIphone(?name, ?mode, ?peer, ?d)$
\end{center}

Note that multiple relations will be populated when this rule fires: assuming the above EDB, we generate the following facts:

\begin{center}
$sendmail@inria.fr(“Alice”, “Happy Birthday”)$
$sms@BobIphone(“Bob”, “Happy Birthday”)$
\end{center}

Note that some protocol is needed here to send this information to other peers. The term the paper uses is delegation.

This system also supports negation. To demonstrate this consider this additional rule:

\begin{center}
	$birthday@myIphone(string, relation, peer, date)$
\end{center}


Here string, relation, peer and date are types. We might write the following rules to fit this schema:

\begin{center}
$birthday@myIphone(?n, ?m, ?p, ?d)$ :- $birthdates@myIphone(?n, ?d), contact@myIphone(?n, ?m, ?p).$
\vspace{5mm}

$birthdates@myIphone(?n, ?d)$ :- $birthdates@myIphone(?n, ?d), not del.birthdates@myIphone(?n, ?d).$
\end{center}

We can understand the del relation here as possibly some information received from another user about whether other birthdates need to be invalidated, or special information given by our user that certain birthdays should not be held.

It is unclear how negation can be used, the WebDamLog paper claims WF semantics is compatible but only prove it is compatible with stratified.

Our initial model of execution is the following:

\begin{itemize}
\item Consume local facts
\item Derive new local facts - any rule with EDBs in the head can derive new local facts.
\item Derive nonlocal facts (messages are sent to other peers here)
\item Modify other peer’s programs (install rules at other peers)
\end{itemize}

It may be surprising that rules need to be installed at other peers. To demonstrate this assume the following rule is installed at p:

\begin{center}
$m@q(?y) :- m1@p(?x), m2@p’(?x, ?y)$
\end{center}

Assume we have $m1@p(a)$ as a fact. When evaluating the rule we look at the atoms in order. Note that this is key to how evaluation works, unlike any of the other datalog variants we have seen so far.

After considering m1@p(a) we get the following:
\begin{center}
	$ m@q(?y) :- m2@p’(a, ?y).$
\end{center}

This rule needs to be installed at p’. Now let’s say we have m2@p’(a, b). Then this rule gives us m@q(b).
What this means depends on whether m is an IDB or EDB.
If it is an EDB we get m@q(b) sent as a fact (similar to sms/sendmail), but if it is an IDB we need to send it as a rule m@q(b) :- .

To further formalize execution we need to talk more about distinctions we can make about rules.

Rules can be either deductive (meaning the head is an IDB), or active (meaning the head is an EDB). They can also be fully local (all atoms are local), local (all atoms except the head are local), or neither.

Restating how execution works, we consider the rules in this order:

\begin{itemize}
\item Local deduction (uses only fully local, deductive rules - plain Datalog)
\item Update active, fully local rules
\item View delegation: deductive rule that is local but not fully local. (e.g. the Happy Birthday message rule). Called view delegation because it gives other users a “view” of the local data.
\item General delegation: all remaining rules. These rules require us to install rules at other peers.
\end{itemize}

Note that view delegation gives extra power compared to SWL, general delegation gives even more.
