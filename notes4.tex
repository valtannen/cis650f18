\section{Lecture: Tuesday, September 11th}
Note taker: Sulekha Kulkarni
\label{sec:LC4}

\subsection{Execution Time of the Naive Evaluation Algorithm}
\label{sec:LC4:ExecNaive}

We discussed the naive Datalog evaluation algorithm in Section~\ref{sec:LC1:OpSem}.
The least fixpoint is computed after finitely many steps. This algorithm produces
$\emptyset \subseteq f(\emptyset) \subseteq f^2(\emptyset) \ldots $ and stops when
$f^n(\emptyset) = f^{n+1}(\emptyset)$.
We claim that this algorithm executes finitely many steps because $E$ is finite.

\begin{defn}[Active Domain] \label{def:LC4:ADom}
$D = activedomain(E) = \{a \mid \exists y \; Eay\} \cup \{b \mid \exists x \; Exb\}$.
\end{defn}

\begin{lemma} \label{lemma:LC4:TFin}
$\forall n \; T_n \subseteq D \times D$.
\end{lemma}

\begin{proof}
We prove by induction on $n$. The base case where $n = 0$ is trivial because $T_0 = \emptyset$ and
this belongs to $D \times D$. For the inductive step where $n > 0$, the IH is $T_k \subseteq D \times D$.
We want to show that $T_{k+1} \subseteq D \times D$. This holds because $T_{k+1} = f(T_k)$ and
$f: (D \times D) \rightarrow (D \times D)$.
\end{proof}

Since $D \times D$ is finite and $T_n \subseteq (D \times D), T_n$ is finite. In the worst case,
the maximum number of steps taken by the naive evaluation algortihm is $\mid D \times D \mid$.
If $\mid D \mid = n$, then $\mid T \mid$ is atmost $n^2$. $\mid T_k \mid$ is polynomial in $\mid E \mid$.
In general, a Datalog program terminates in polynomial number of steps with an answer that is polynomial
in the size of the EDB.

\subsection{Drawbacks of the Naive Evaluation Algorithm}
\label{sec:LC4:DbkNaive}

\begin{figure}[t]
\begin{center}
        \includegraphics[scale=0.6]{images/graph} \\
\end{center}
\hrule width \hsize height .33pt
\vspace{0.03in}
\caption{Example graph for tracing the naive evaluation algorithm.}
\label{fig:LC4:gr}
\end{figure}

We examine the execution of the naive evaluation algorithm on the EDB $\{(a,b), (b,c), (c,c), (c,d)\}$.
The EDB represents the edges in the graph in Figure~\ref{fig:LC4:gr}.
Starting with $T_0 = \emptyset$, each successive iteration of the naive evaluation algorithm produces the
sets $T_1, T_2, \ldots$ in iterations 1,2,... as shown below.
\begin{itemize}
\item Iteration 1 computes $(a,b), (b,c), (c,c)$ and $(c,d)$.
\item Iteration 2 computes $(a,b), (b,c), (c,c), (c,d), (a,c), (b,c), (b,d), (c,c)$ and $(c,d)$. Note that only $(a,c)$ and 
$(b,d)$ are new elements. The elements $(b,c), (c,c)$ and $(c,d)$ are already present, but are recomputed.
\item Iteration 3 computes $(a,b), (b,c), (c,c), (c,d), (a,c), (a,d), (b,c), (b,d), (c,c)$ and $(c,d)$. This time only the 
element $(a,d)$ is new and the elements $(a,c), (b,c), (b,d), (c,c)$ and $(c,d)$ are recomputed.
\item Iteration 4 does not compute any new elements. Therefore the algorithm stops after iteration 4.
\end{itemize}
We observe that the drawback of the naive evaluation algorithm is that it recomputes elements multiple times.

\subsection{The SemiNaive Evaluation Algorithm}
\label{sec:LC4:SemiNaive}

We use the following informal notation to illustrate this algorithm.
\begin{align*}
T &\coloneq E \\
T &\coloneq T;T
\end{align*}
We denote by $\delta T_i$ the new tuples generated in iteration $i$. Therefore, $T_{i+1} = T_i \cup \delta T_i$.
Therefore, $T_0 = \emptyset$, $\delta T_0 = E$ and $T_1 = T_0 \cup \delta T_0 = E$.
For a general term $T_{i+1}$,
\begin{align*}
T_{i+1} &= T_i;T_i \\
        &= (T_{i-1} \cup \delta T_{i-1});(T_{i-1} \cup \delta T_{i-1}).\\
        &= (T_{i-1};T_{i-1}) \cup (T_{i-1};\delta T_{i-1}) \cup (\delta T_{i-1};T_{i-1}) \cup (\delta T_{i-1};\delta T_{i-1}).
\end{align*}
The first term above equals $T_i$. We denote the union of the last three terms by $\Delta T_i$. That is,
$\Delta T_i = (T_{i-1};\delta T_{i-1}) \cup (\delta T_{i-1};T_{i-1}) \cup (\delta T_{i-1};\delta T_{i-1})$.
Therefore, $T_{i+1} = T_i \cup \Delta T_i$.

Writing the same thing in a Datalog-like notation,
\begin{align*}
\Delta T_i &\coloneq T_{i-1};\delta T_{i-1} \\
\Delta T_i &\coloneq \delta T_{i-1};T_{i-1} \\
\Delta T_i &\coloneq \delta T_{i-1};\delta T_{i-1} \\
\end{align*}

We compute $\delta T_i$ as $\Delta T_i \setminus T_i$. \\

Now, we give the semi-naive evaluation algorithm:

\begin{enumerate}[noitemsep, topsep=0pt, label={}]
\item $for\; each$ IDB predicate $T$
   \begin{enumerate}[noitemsep, topsep=0pt, label={}]
   \item $T_0 = \emptyset$
   \item $\delta T_0 = E$
   \end{enumerate}
\item $ repeat $
   \begin{enumerate}[noitemsep, topsep=0pt, label={}]
   \item $ T_i = T_{i-1} \cup \delta T_{i-1} $
   \item evaluate $\Delta T_i$
   \item $ \delta T_i = \Delta T_i \setminus T_i $
   \item $ i = i + 1 $
   \end{enumerate}
\item $ until \; \delta T_i == \emptyset $
\end{enumerate}
