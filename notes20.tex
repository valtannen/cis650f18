\section{Lecture: Tuesday, November 20th}
Note taker: Cyril Saade
\label{sec:LC20}

\subsection{Decidability}
As a review, there exists conjunctive queries (CQ), Union Conjunctive Queries (UCQ), and Datalog (which represent UCQ with recursion). Last time, we saw that equivalence between two Datalog program is undecidable. In this lecture, we will see that CQ \& UCQ are decidable and that they are in fact NP-Complete.




\subsection{Datalog Uniform is Decidable}

Let us first explore the difference between equivalence and containment. The definition of containment is as follows:
$$q_1 \sqsubseteq q_2\ \forall{I} \ q_1(I) \subseteq q_2(I)$$

The definition of equivalence is as follows: 
$$ q_1\equiv q_2 \ \forall{I} \ q_1(I) = q_2(I)$$

Therefore, we can say that $q_1 \equiv q_2$ if and only if $q_1 \sqsubseteq q_2$.
Note: containment is important because CQ has been optimized. Thus, containment helps us demonstrate that an optimized query is equal to the original one.\newline

Let us define a Datalog instance $\cal{E}$ of edb predicates. $\cal{E} \mapsto{^P} I$. This means that we start with whatever we have in the edb predicates and apply Datalog semantics to; as a result, we thus look at the idb instances. 

If $P_1 \equiv P_2$, this means that $\forall{\cal{E}} \ P_1({\cal{E}})=P_2({\cal{E}})$; we have shown that this is undecidable.\newline

Let us change perspective and say that we start with the input such that:
$\cal{E}, I \mapsto{^P} I'$; we will still look at the resultant idb predicates.

As an example, we can demonstrate the latter on our known transitive closure program.
\begin{align*}
T_{xy} &:- E_{xy} \ \ \ \ we \ start\ with\ E,\ T\ =\  \emptyset \\
T_{xz} &:- E_{xy}, T_{yz}
\end{align*}

On the other hand, we can also have:

\begin{align*}
T_{xy} &:- E_{xy} \ \ \ \ we \ start\ with\ E,\ =\  \emptyset \ T=edges \ in \ digraph \\
T_{xy} &:- T_{xz}, T_{zy}
\end{align*}

We claim that the latter program will compute the transitive closure of all the edges in the graph. If our graph is $G=(V,E')$, then $T=E'$ initially. Thus, our transitive closure is initially correct.

\begin{defn}[Atom]
$P_1 \equiv^{w} P_2$ : $\forall{\cal{E}, I} \ P_1(\cal{E}, I) = P_2(\cal{E}, I)$. We call this uniform equivalence. Uniform equivalence is strictly stronger than equivalence. We can thus also say that: $U\ Equiv \implies Equiv$.
\end{defn}

However, the converse isn't true. For example, if we take $E=0$, $T=E'$ in the program above; nothing will happen (since $E=0$). Therefore, $Equiv$ does not imply $U\ Equiv$..

We can thus hope that uniform equivalence is decidable. In order to prove the latter, we will dive into details of database theory.
\begin{enumerate}
\item {First, we will talk about conjunctive query containment and minimization (optimization method in which we remove redundant atoms in the body; since the fewer atoms we have in the body, the fewer the joins and the faster the query).}
\item Second, we will introduce the Chase Method.
\end{enumerate}

\subsection{Conjunctive Query Containment and Minimization}
Suppose we have two queries (in which u, and $\forall{i} \ x_i$ may be multiple variables):

\begin{align*}
&(q) \ ans(u):- R_1x_1, ..., R_nx_n \\
&q(\cal{E}) = \{ \text{v(u)} | \text{v: Var(q)} \rightarrow D \text{s.t.} R_{iv}(x_i) \in \cal{E}\}
\end{align*}

We define \cal{E} as the input database: a database that contains instances that follow the definition of the program above. We also define $v(x_i)\in R_i$.

Note: for every output $(vu)$, we have to find a valuation; $q(\cal{E})$ is a set of all such valuations.

\begin{theorem} Homomorphism Theorem:

$q_1 \sqsubseteq q_2$ if and only if there is some homomorphism $h:body(q_1)\mapsto body(q_2)$ that preserves the variables in the head of the queries. We note that the homomorphism object is finite. Therefore, if for a query we can find its homomorphism, this means that the latter is decidable.
\end{theorem}

As an example, we can define the following two queries ($q_1$ and $q_2$):
\begin{align*}
&(q_2) \ ans(u) :- R_1x_i, ..., R_nx_n \\
&(q_2) \ ans(u) :- S_1y_1, ..., S_ny_m
\end{align*}
Therefore, the homomorphism function for both queries can be defined as: 
$h: x_1\cup .. \cup x_n \mapsto y_1 \cup ... \cup y_m$ $\forall{i}, \exists{j} \ h(R_ix_i)= S_jy_j$ $R_i\equiv S_j$ $h(x_i)=y_j$.

The following example would sound more concrete:
\begin{align*}
&(q_2) \ ans(xy) :- E_{xz}, E_{zy} \\
&(q_1) \ ans(xy) :- E_{xz}, E_{zy}, E_{xz_2}, E_{z_2y}
\end{align*}
Let's think about this in terms of edges in a graph. There must be a pattern like this:


\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,
                    thick,main node/.style={circle,draw,font=\sffamily\Large\bfseries}]

  \node[main node] (1) {x};
  \node[main node] (2) [right of=1] {};
  \node[main node] (3) [right of=2] {y};
  \node[main node] (4) [right of=3] {};

  \path[every node/.style={font=\sffamily\small}]

    (1) edge node [right] {} (2)
        edge node {} (2)
    (2) edge node [right] {} (3)
   	edge [loop above] node {} (2)
        edge node {} (3)
    (3) edge node [right] {} (4)
        edge node {} (4);
\end{tikzpicture}

Everything in $q_1$ is computable by $q_2$. Therefore, $q_1\sqsubseteq q_2$. Let's find the homomorphism from $q_2$ to $q_1$.

For  $q_2$:
\begin{align*}
x&\mapsto x \\
y&\mapsto y \\
z&\mapsto z_1
\end{align*}
Thus, $E_{xz} \mapsto E_{xz_1}$, $E_{zy}\mapsto E_{z_1y}$. Therefore, $q_1\sqsubseteq q_2$.

We can also go from $q_1$ to $q_2$:
\begin{align*}
&x\mapsto x \\
&y\mapsto x \\
&z_1\mapsto z \\
&z_2\mapsto z
\end{align*}
Thus, $q_2\sqsubseteq q_1$.

Therefore, $q_1\equiv q_2$. In other words, both queries compute the same thing, but $q_1$ is less efficient than $q_2$. This is an example of optimization by minimization: we are able to eliminate redundant atoms in $q_1$ and minimize it to $q_2$.

The problem is, given $q_1$, we can find redundant atoms. In other to do this, we can find a homomorphism from $q_1$ to $q_2$: $h:\ Var(q_1)\mapsto Var(q_2)$. So we get:
\begin{align*}
&x\mapsto x \\
&y\mapsto x \\
&z_1\mapsto z_1 \\
&z_2\mapsto z_2
\end{align*}

This tells us that $E_{xz_2}$, $E_{z_2y}$ are redundant.

In other words, as a more formal definition, given $h:\ x_1\cup .. \cup x_n$, $Range(h) \not\sqsubseteq R_ix_i$., then we can eliminate $R_ix_i$. We can define q: $ans(u):- R_1x_1, ..., R_nx_n$ and q': $q \setminus R_ix_i$. Then, by definition $q\equiv q'$.


Note: In general, we can start with a query and keep minimizing it until we can't minimize further. This minimization will always give us a unique answer.


\subsection{Union Conjunctive Query Containment and Minimization}

Let's work on an example. We define $q_2$ as the following:
\begin{align*}
&ans(xy) :- E_{xz}, E_{zy}, E_{yw} \\
&ans(xy) :- E_{vx}, E_{xz}, E_{zy}
\end{align*}
We can also define another query, $q_1$ $ans(xy):- E_{vx}, E_{xz}, E_{zy}, E_{yw}$.


\begin{claim}$q_1\sqsubseteq q_2$. The homomorphism in UQ is straightforward from the one in QC.\end{claim}

Proof that \textsc{Finding a homomorphism} $\in$ NP.
Given a $A\mapsto B$ function , where $|A|=n$ and $|B|=m$. We have $n^m$ mappings. Therefore, given a function $x$, we can verify it in polynomial time. To show that \textsc{containment in UCQ} is NP-Complete, it is enough to show that \textsc{containment in CQ} is NP-Complete.\newline

In order to show that the uniform equivalence of Datalog is decidable, we will introduce the Chase Method later on in the course.
