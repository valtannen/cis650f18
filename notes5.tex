\section{Lecture: Thursday, September 13th}
Note taker: Gianluca Gross
\label{sec:LC5}

\subsection{Equivalence of Datalog Semantics}
\label{sec:LC5:EquivSem}

We have already discussed the operational semantics and denotational semantics of Datalog. We focused particularly on the two transitive closure programs discussed in Section~\ref{sec:LC2:TransClos}. We discussed both the naive and seminaive algorithms to compute the results of those programs (operational semantics) and also discussed the programs' relations to least fixpoints (denotational semtantics). We introduced the Knaster-Tarski theorem to discuss least fixpoints and used the Kleene fixpoint theorem to connect the transitive closure algorithms and least fixpoints, and thus proved the equivalence of the operational and denotational semantics of Datalog.

Soon we will introduce another type of semantics: proof-theoretic semantics. Our aim is to show that the proof-theoretic semantics are also equivalent to the operational and denotational semantics of Datalog.

But first, we will take a quick look at some extensions to Datalog as well as the properties of the active domain of a Datalog program.

\subsection{Extensions to Datalog}
\label{sec:LC5:DatalogExt}

We will briefly mention some extensions to the Datalog programming language the currently exist. We are working with the ``vanilla'' version of Datalog for now, so we will not work extensively with these additions yet, but they are worth mentioning.

Two notable extensions are aggregation and arithmetic, which can make Datalog programs very powerful.

It is also possible to introduce constants into Datalog programs. For example, consider the following program:
\begin{align*}
Txy &\coloneq Exy \\
Txy &\coloneq Exz, Tzy \\
Rx  &\coloneq Tax
\end{align*}
Here, $a$ is a constant which is in the active domain of $E$. If we think of $E$ as a set of directed edges and think of $T$ as representing reachability, then $R$ will contain all nodes which are reachable from $a$.

\subsection{Properties of the Active Domain}
\label{sec:LC5:ActDomProp}

Suppose $E$ is an EDB predicate in a Datalog program and we want to compute the active domain $D$ of the EDB (i.e. $D = adom(E)$). We can do so with the following Datalog program:
\begin{align*}
Dx &\coloneq Exy \\
Dy &\coloneq Exy
\end{align*}
NOTE: A small detail of note here is that $D = adom(E)$ is a set while the $D$ in the above program is actually an IDB predicate. So technically, the two are not equivalent, but the $D$ in the program is really the name of the output which will contain the set $adom(E)$.

In general, we can think of the active domain with respect to an entire Datalog program. That is, if $P$ is a Datalog program, then the active domain of $P$ (i.e. $D = adom(P)$) is the union of the active domains of all EDBs in $P$.

Also, the active domain of a program $P$ will be finite assuming all the EDBs in $P$ are finite.

\subsection{Proof-Theoretic Semantics}
\label{sec:LC5:ProofSem}

We will now discuss the proof-theoretic semantics of Datalog. But first, we must introduce a few definitions.

\begin{defn}[Valuation] \label{def:LC5:Valuation}
Consider a particular rule $r$ in a Datalog program $P$ with active domain $D = adom(P)$. Let $Var(r)$ be the set of variables in $r$ (e.g. $x, y, z$, etc.). Then a function $v : Var(r) \rightarrow D$ is a valuation for $r$.
\end{defn}

\begin{defn}[Rule Instantiation] \label{def:LC5:Instantiation}
An instantiation of a Datalog rule $r$ with respect to a valuation $v$ is a logical statement that results from replacing all variables $x \in Var(r)$ with $v(x)$.

For example, consider the Datalog rule $Txy \coloneq Exz, Tzy$. Suppose $v$ is a valuation such that $v(x) = b$, $v(y) = b$, and $v(z) = c$. Then the instantiation of the rule with respect to $v$ is $Tbb \coloneq Ebc, Tcb$.

NOTE: The terms $Tbb, Ebc,$ and $Tcb$ in this instantiation are called \textit{facts}. In particular, $Ebc$ is an extensional fact because $E$ is an EDB predicate, while $Tbb$ and $Tcb$ are intensional facts because $T$ is an IDB predicate.
\end{defn}

\begin{defn}[Facts] \label{def:LC5:Facts}
In the instantiation $Tbb \coloneq Ebc, Tcb$, the terms $Tbb, Ebc,$ and $Tcb$ are called \textit{facts}. $Ebc$ is an extensional fact because $E$ is an EDB predicate, while $Tbb$ and $Tcb$ are intensional facts because $T$ is an IDB predicate. 
\end{defn}

\begin{defn}[Closed World Assumption] \label{def:LC5:CWA}
The closed world assumption (CWA) in a logic system is the presumption that all relevant information has been specified, and that any fact that has not been specified or cannot be inferred from the given information is false.

We will take the closed world assumption when dealing with facts in Datalog.
\end{defn}

The instantiation $Tbb \coloneq Ebc, Tcb$ is equivalent to the logical statement $Ebc \wedge Tcb \rightarrow Tbb$, which states that $Ebc$ (fact 1) and $Tcb$ (fact 2) imply $Tbb$ (fact 3).

Using this instantiation, it is possible to prove the fact $Tbb$ if we know that $Ebc$ and $Tcb$ hold. Consider the following proof tree that does just this:
\begin{displaymath}
\prftree[r]{\textit{modus ponens}}
  {\prftree{Ebc}{Tcb}{Ebc \wedge Tcb}}
  {Ebc \wedge Tcb \rightarrow Tbb}
{Tbb}
\end{displaymath}

\begin{defn}[modus ponens] \label{def:LC5:modpon}
In propositional logic, \textit{modus ponens} is the rule which states that if $P$ implies $Q$ and $P$ is known to be true, then $Q$ must be true.
\end{defn}

We can follow this general pattern to construct proof trees which use instantiations of Datalog rules and certain assumptions to prove new facts.

However, when proving facts in this fashion, we want to ensure that all of our assumptions at the leaves of a proof tree are \textit{extensional}. In other words, our assumptions need to be facts that are given in an input EDB. In the previous example, the fact $Ebc$ is an extensional assumption because it comes from the EDB $E$. However, the fact $Tcb$ is an intensional assumption that itself needs to be derived because it comes from the IDB $T$. In other words, our proof tree to prove $Tbb$ is incomplete.

Let's go back to our earlier example program (where $a$ is a constant):
\begin{align*}
r1 &: \quad Txy \coloneq Exy \\
r2 &: \quad Txy \coloneq Exz, Tzy \\
r3 &: \quad Rx  \coloneq Tax
\end{align*}
Here, $r1, r2,$ and $r3$ are the names of the three rules of the program. Now suppose $E = \{(a, b), (b, c), (c, c), (c, d)\}$. We want to prove the fact $Rd$ (i.e. $d$ is reachable from $a$) using facts from the EDB $E$ and instantiations of the rules in the Datalog program. We can do so as follows:

\begin{displaymath}
\prftree[r]{r3}
  {\prftree[r]{r2}
    {Eab}
    {\prftree[r]{r2}
      {Ebc}
      {\prftree[r]{r1}
        {Ecd}
      {Tcd}}
    {Tbd}}
  {Tad}}
{Rd}
\end{displaymath}

These sorts of proof trees are indeed the proof-theoretic semantics of Datalog.

We would now like to show that the proof-theoretic semantics are equivalent to the operational and denotational semantics of Datalog. In particular, we will do this by showing that the proof-theoretic semantics are equivalent to the denotational semantics.

\subsection{Equivalence of Proof-Theoretic Semantics}
\label{sec:LC5:ProofSemEquiv}

In proving the equivalence of the proof-theoretic and denotational semantics, we will focus on the transitive closure program:
\begin{align*}
r1 &: \quad Txy \coloneq Exy \\
r2 &: \quad Txy \coloneq Exz, Tzy 
\end{align*}
Consider the set $T_{proof} = \{(u, v) \mid \text{there exists a proof tree that yields $Tuv$}\}$, which is the set of tuples in $T$ that are derivable through the proof-theoretic semantics.

We represented the denotational semantics of this program as the function $f(T) = E \cup E;T$, and defined $T_0 = \emptyset$ as well as $T_{k+1} = E \cup E;T_k$. Furthermore, we used the Kleene fixpoint theorem to show that the set of tuples derivable through the denotational semantics is $lfp(f) = \bigcup_{i \geq 0} {T_i}$.

We want to show that the set of tuples which are derivable through the proof-theoretic semantics is equivalent to the set of tuples which are derivable through the denotational semantics. In other words, we want to prove:
$$T_{proof} = \bigcup_{i \geq 0} {T_i}$$
We will do this by proving two lemmas.

\begin{lemma} \label{lemma:LC5:ProofEquivLemma1}
$\bigcup_{i \geq 0} {T_i} \subseteq T_{proof}$
\end{lemma}

\begin{proof}
We will prove the lemma by proving that $\forall i \geq 0, T_i \subseteq T_{proof}$ through induction on $i$. 

The base case where $i = 0$ holds because $T_0 = \emptyset$, and so $T_0 \subseteq T_{proof}$ surely holds. 

For the inductive step where $i > 0$, the IH is $T_k \subseteq T_{proof}$. In other words, this tells us that every tuple $(u, v) \in T_k$ has a proof tree.

We want to show that $T_{k+1} (\emptyset) \subseteq T_{proof}$. Consider any tuple $(w, v) \in T_{k+1}$. We want to show that $Twv$ has a proof tree. By definition, we know that $T_{k+1} = E \cup E;T_k$. Since $(w, v) \in T_{k+1}$, we know that $(w, v) \in E \cup E;T_k$. There are two cases to consider.

\underline{Case 1:} $(w, v) \in E$. In this case, we know that $(w, v)$ is in the EDB $E$, and so $Ewv$ holds. Then we can prove that $Twv$ holds with following simple proof tree.

\begin{displaymath}
\prftree[r]{r1}{Ewv}{Twv}
\end{displaymath}

\underline{Case 2:} $(w, v) \in E;T_k$. By the definition of relation composition, we know that $\exists u \in D$ such that $(w, u) \in E$ and $(u, v) \in T_k$. Since $(w, u) \in E$, we know that the fact $Ewu$ holds. Also, since $(u, v) \in T_k$, we know from the inductive hypothesis that there is a proof tree which proves $Tuv$. Denote this proof tree as:

\begin{displaymath}
\prftree[r]{\text{IH}}{***}{Tuv}
\end{displaymath}

Using this, we can construct a proof tree for $Tuv$ as follows:

\begin{displaymath}
\prftree[r]{r2}
  {Ewu}
  {\prftree[r]{\text{IH}}{***}{Tuv}}
{Twv}
\end{displaymath}

This concludes the proof that $\bigcup_{i \geq 0} {T_i} \subseteq T_{proof}$.
\end{proof}

\begin{lemma} \label{lemma:LC5:ProofEquivLemma2}
$T_{proof} \subseteq \bigcup_{i \geq 0} {T_i}$
\end{lemma}

This can be restated as the following: For any proof tree, its conclusion is in $\bigcup_{i \geq 0} T_i$.

NOTE: It is important to note that an intensional fact can have more than one proof tree.

\begin{proof}
The proof of this lemma proceeds by induction on the height of the proof tree. However, we will not discuss the details here.
\end{proof}