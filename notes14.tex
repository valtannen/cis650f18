\section{Lecture: Tuesday, October 23rd}
Note taker: Yinjun

Today, we mainly focus on various kinds of $Datalog^{\neg}$, which have different semantics. For each $Datalog^{\neg}$, we want to care about whether it satisfies 1) operational semantics (kleene fixed point theorem); 2) denotational semantics (Knaster-Tarski fixed point theorem and minimal model theorem); 3) proof-theoretic semantics

\subsection{semi-positive $Datalog^{\neg}$}
Semi-positive $Datalog^{\neg}$ is a class of Datalog queries where negation only appears in EDB predicates. Due to the existence of negation, Semi-positive $Datalog^{\neg}$ may not be monotone and thus kleene fixed point theorem and Knaster-Tarski fixed point theorem cannot work.

\subsection{stratified $Datalog^{\neg}$}
Stratified $Datalog^{\neg}$ is a class of Datalog queries where 1) IDB predicates can involve negation; 2) for a Datalog rule with negation, the attributes in negative predicates should appear in other positive predicates; 3) the entire Datalog programs can be stratified such that the Datalog rules only with positive predicates are evaluated first and then other rules with negative predicates are evaluated.

The first two conditions can be checked by simply inspecting each rule while the third condition should be checked by dependency graph (with possible negation label on edges) mentioned in Section \ref{ssec: recursive}. 

Consider the following Datalog program as an example:
\begin{align*}
Txy &\coloneq Exy & (1) \\
Txy &\coloneq Exz, Tzy & (2) \\
Ax &\coloneq Exy & (3) \\
Ay &\coloneq Exy & (4) \\
Sxy &\coloneq \neg Txy, Ax, Ay & (5) \\
\end{align*}

In this example, we want to compute all the $(x,y)$ pair that does not appear in $T$, which is achieved by negating $T$ in rule (5) but with restricted ranges in the active domain of $E$ (defined by $A$). We can show that this Datalog program is a {\em stratified $Datalog^{\neg}$ program}. The first two conditions are satisfied since only rule (5) has negation in which $T$ is negated and the attributes $x$ and $y$ of $T$ appear in other predicates in this rule. The third conditions can be checked by building a {\em dependency graph} in Figure \ref{fig:dependency_graph_cycle_stratified}.

\begin{figure}[H]
\begin{center}
\begin{tikzpicture}[grow'=up,scale=0.75]
\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
\tikzset{edge/.style = {->,> = latex'}}
% vertices
\node[vertex] (1) at (0,0) {$T$};
\node[vertex] (2) at (2,0) {$A$};
\node[vertex] (3) at (1,-2) {$S$};
%edges
% \draw[edge] (1) to (3);
\draw[->] (1) --(3) node[pos=.5,above]{$\neg$}; 
\draw[edge] (2) to (3); 
\draw [->] (-0.4,0.3) arc (10:330:20pt);
\end{tikzpicture}
\end{center}
\caption{Dependency graph for stratified program test}
\label{fig:dependency_graph_cycle_stratified}
\end{figure}

Note that in this dependency graph, the edge from $T$ to $S$ is labeled with $\neg$, which indicates that $T$ is negated in the body of $S$. Since there is no cycles with negation label in this graph, this program should be stratified.

Intuitively, for stratified $Datalog^{\neg}$, we can consider the rules only with positive predicates as one stratum while for the other rules with negative predicates as another stratum such that we can evaluate the rules in the first stratum and then view the predicates in that stratum as ``EDB'' predicates, which are then used to evaluate the rules in the second stratum. This is so called {\em stratum of semi-positive Datalog}.


However, for other Datalog programs, such as the one below, they cannot pass the dependency graph test and thus cannot be be stratified.

Consider the following Datalog program:
\begin{align*}
Sx &\coloneq Ax, \neg Rx & (1) \\
Rx &\coloneq Ax, \neg Sx & (2)
\end{align*}

Its dependency graph is as follows (See Figure \ref{fig:dependency_graph_cycle_non_stratified}):
\begin{figure}[H]
\begin{center}
\begin{tikzpicture}[grow'=up,scale=0.75]
\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
\tikzset{edge/.style = {->,> = latex'}}
% vertices
\node[vertex] (1) at (0,0) {$R$};
\node[vertex] (2) at (2,0) {$S$};
%edges
% \draw[edge] (1) to (3);
\path[->]
(1) edge[bend left] node[swap] {$\neg$} (2)
(2) edge[bend left] node {$\neg$} (1);
% \draw[->] (1) --(3) node[pos=.5,above]{$\neg$}; 
% \draw[edge] (2) to (3); 
% \draw [->] (-0.4,0.3) arc (10:330:20pt);
\end{tikzpicture}
\end{center}
\caption{Dependency graph for non-stratified program}
\label{fig:dependency_graph_cycle_non_stratified}
\end{figure}

As Figure \ref{fig:dependency_graph_cycle_non_stratified} shows, there is a cycle with edges labeled with negation. So this program cannot be stratified.

In terms of the semantics for this kind of non-stratified Datalog program, Kleene fixed point theorem and Knaster-Tarski fixed point theorem cannot be satisfied since monotonicity is not satisfied, which is proved as follows:

We can rewrite this Datalog program as a form of function which has two attributes ($Rx$ and $Sx$) as output, i.e. $f(R, S) = (A$\textbackslash$S, A$\textbackslash$R)$. Then we can start with bottom case $\bot = (R, S) = (\phi, \phi)$ and $f(R, S) = (A, A)$. But $f(f(R, S)) = (\phi, \phi)$, which means that $\bot, f(\bot),\dots,f^{n}(\bot)$ is not monotone and not a $\omega$-$chain$. So we cannot get a fixed point for this non-stratified $Datalog^{\neg}$.


Besides, the minimal model also fails. For example, suppose the EDB predicate $A$ has one fact $\{1\}$, we can have two different models that satisfy the Datalog program above, i.e. $M_1: A=\{1\}, R=\{1\}, S=\{\}$ and $M_2: A=\{1\}, R=\{\}, S=\{1\}$. We cannot find another model that satisfies the Datalog program and is included by each of the two models. However, neither of the two models can be included by the other, which violates the requirements for minimal model.


\subsection{inflationary $Datalog^{\neg}$}
In order to make non-stratified $Datalog^{\neg}$ reach a fixed point where no new facts are derived. We can introduce {\em inflationary operator}. Still consider the program:
\begin{align*}
Sx &\coloneq Ax, \neg Rx & (1) \\
Rx &\coloneq Ax, \neg Sx & (2)
\end{align*}

Recall that this program is not stratified and can be written as $f(R, S) = (A$\textbackslash$S, A$\textbackslash$R)$. We can create one {\em inflationary} version of function $f$, i.e. $f_i(R,S) = (R \bigcup A$\textbackslash$S, S \bigcup A$\textbackslash$R)$, which means that {\em we keep all the facts that we have derived so far in the final output}.

For $f_i$, we can prove that it has a fixed point. Starting from $\bot=(\phi,\phi)$, $f_i(R, S)=(A,A)$, $f_i(f_i(R,S)) = (A,A)$ and thus $f_i(f_i(....(R,S)))=(A,A)$. This is the simpliest way to define fixed point semantics for Datalog with negation.

\subsection{Well-found $Datalog^{\neg}$}
There is another type of $Datalog^{\neg}$ called {\em Well-found $Datalog^{\neg}$}, which introduce another type of logic including {\em Undefined} for uncertain case (besides True and False).

Note that in practice, stratified $Datalog^{\neg}$ is used most frequently although it is not expressive as inflationary $Datalog^{\neg}$ and Well-found $Datalog^{\neg}$

\subsection{System of equation}
The Datalog programs (with a set of relation $\Bar{R}=\{R_1, R_2,\dots, R_n\}$) can be also expressed as a system of equations, i.e.:

\[
\begin{cases} R_1 = \phi_1(\Bar{R}) \\ R_2 = \phi_2(\Bar{R}) \\R_3 = \phi_3(\Bar{R})\\...\\R_n = \phi_n(\Bar{R})\end{cases}
\]

In such case, $\phi_i(\Bar{R})$ can be FOL queries with any quantifiers. For example, $\phi_i(\Bar{R})$ can be $Gx = \forall y(Exy \rightarrow Gy)$, which represents all the nodes in the graph without cycle.

Note that for the system of equations above, the FOL queries and Fixed points can be combined, in which case inflationary operations and monotoncity have the same expressive power. Plus, FOL queries with Fixed points can be captured in PTIME on database with order, which indicates that any queries that can be evaluated within PTIME can be expressed by FOL with Fixed points.