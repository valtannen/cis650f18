\section{Lecture: September 27th}

Note Taker: Richard Zhang

In the last lecture, we presented a way to evaluate Datalog programs using ground refutation via SLD-resolution. Ground refutation is sound and complete, but has a key flaw: it requires us to concretize every rule with every single possible assignment of variables. This is impractical when the input database (EDB) is even moderately large.

In this lecture we will present a variant of SLD-resolution that avoids this problem.

\subsection{Example}

Consider the following program:
\begin{align*}
Txy &\coloneq Exy & (1) \\
Txy &\coloneq Exz, Tzy & (2) \\
Rx  &\coloneq Txy, Eyy & (3) \\
\end{align*}

$T$ is the usual transitive closure, $R$ represents where $x$ is transitively connected to a point that links to itself. Our EDB is going to be the usual graph as in Figure \ref{fig:LC4:gr}.

In ground resolution, we tried to prove facts like $Ra$. Let's instead try to prove $Ru$, where $u$ is a variable.

\begin{align*}
\neg Ru \quad & x \rightarrow u, y \rightarrow v & \text{unification with } (3) \\
\downarrow & & \\
\neg Tuv \vee \neg Evv \quad & x \rightarrow u, y \rightarrow v & \text{unification with }(2) \\
\downarrow & & \\
\neg Euz \vee \neg Tzv \vee \neg Evv \quad & x \rightarrow z, y \rightarrow v & \text{unification with (2)} \\
\downarrow & & \\
\neg Euz \vee \neg Ezz' \vee \neg Tz'v \vee \neg Evv \quad & x \rightarrow z', y \rightarrow v & \text{unification with (1)} \\
\downarrow & & \\
\neg Euz \vee \neg Ezz' \vee \neg Ez'v \vee \neg Evv \quad & v \rightarrow c & \text{unification with EDB} \\
\downarrow & & \\
\neg Euz \vee \neg Ezz' \vee \neg Ez'c \vee false
\end{align*}

The intuition here is to follow the same process as ground refutation, except instead of substituting variables for constants, we also sometimes substitute variables for variables.

We've reached a point where we can now only do substitutions on EDB facts. There are several paths we can choose: consider the following three proof trees.

\begin{align*}
\neg Euz \vee \neg Ezz' \vee \neg Ez'c \vee false & \quad z' \rightarrow b \\
\downarrow & \\
\neg Euz \vee \neg Ezb \vee false \quad \vee false & \quad z \rightarrow a \\
\downarrow & \\
\neg Euz \vee false \vee false \vee false & \\
\end{align*}

With this tree, we've reached a blocking refutation: no more progress can be made.

\begin{align*}
\neg Euz \vee \neg Ezz' \vee \neg Ez'c \vee false & \quad z' \rightarrow c \\
\downarrow & \\
\neg Euz \vee \neg Ezc \vee false \vee false & \quad z \rightarrow b \\
\downarrow & \\
\neg Eub \vee false \vee false \vee false & \quad u \rightarrow a \\
\downarrow & \\
false \vee false \vee false \vee false
\end{align*}

Here we derive $Ra$, since we were originally trying to show $Ru$ and we substituted $u$ for $a$.

\begin{align*}
\neg Euz \vee \neg Ezz' \vee \neg Ez'c \vee false & \quad z' \rightarrow c \\
\downarrow & \\
\neg Euz \vee \neg Ezc \vee false \vee false & \quad z \rightarrow c \\
\downarrow & \\
\neg Euc \vee false \vee false \vee false & \quad u \rightarrow b/c \\
\downarrow & \\
false \vee false \vee false \vee false
\end{align*}

Note that here we can derive both $Rb$ and $Rc$ for a similar reason.

\subsection{Unifiers}

How do we formalize the intuition we presented above? We need some theory of unification.

\begin{defn}[Atom] \label{def:LEC8:atom}
	A term of the form $R\bar\xi$, where $\bar\xi$ is a list of both variables and constants.
\end{defn}

\begin{defn}[Unifier] \label{def:LEC8:unifier}
Given atoms $A, B$, a unifier from $A, B$ is a substitution $\theta$ so that $\theta(A) = \theta(B)$, where $\theta : Var \rightarrow Var \bigcup Const$.	
\end{defn}

We now define generality of unifiers. Intuitively, more general unifiers identify fewer variables.

\begin{defn}[Generality of Unifiers] \label{def:LEC8:general}
$\theta \lesssim \theta'$ if $\exists v$ where $\theta = v ; \theta'$.
\end{defn}

As an example of generality, consider $A1x_1x_2$ and $Ax_3x_4x_5$. These two atoms are unified by $\theta(x_1) = \theta(x_2) = \theta(x_4) = \theta(x_5) = y, \theta(x_3) = 1$. However, intuitively this forces $x_1 = x_2$ when that isn't necessarily true in the original atom. We could have instead unified via $\theta'(x_3) = \theta'(x_1) = y$, $\theta'(x_2) = \theta'(x_4) = z$, $\theta'(x_5) = 1$. Note that taking $v$ to be $v(z) = y$, $\theta = \theta';v$.

Note that generality is a preorder: it is both reflexive and transitive.

\begin{defn}[Most General Unifier (mgu)] \label{def:LEC8:mgu}
Given unifiers A and B, an mgu for A and B is a unifier $\mu$ s.t. for any other unifier $\theta \lesssim \mu$.	
\end{defn}

Note that the mgu always exists but is not necessarily unique.

\subsection{Big Picture}

This gives us the algorithm for refutation:

\begin{enumerate}
\item Choose some $\neg A_i$ and find a rule R where $A_i$ and the head of R (call it $B$) are unifiable.
\item Find the most general unifier $\theta$ for $A_i$ and $B$.
\item Replace $\neg A_i$ with the body of the rule, and apply $\theta$ to all $A_k$ and $B$
\end{enumerate}

Note that the mgu is required to ensure completeness.

