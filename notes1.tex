\section{Lecture: Thursday, August 30th}
Note taker: Sulekha Kulkarni
\label{sec:LC1}

\subsection{Syntax of Datalog}
\label{sec:LC1:Syntax}

A Datalog program comprises a set of rules of the form $T(x, y) \coloneq E(x, y)$.
Here, $T(x, y)$ and $E(x, y)$ are both \emph{predicates} or relations of arity 2.
$T$ is the \emph{name} of the predicate and the set of all pairs $(x, y)$ that belong
to the predicate is the \emph{extent} of the predicate.
$x$ and $y$ are variables that can take values from $D$ which we define to be the active
domain of constants.
Only one predicate can occur to the left of the $\coloneq$ sign in a Datalog rule and this
predicate is called the \emph{head} of the rule.
One or more predicates can occur to the right of the $\coloneq$ sign and they form the 
\emph{body} of the rule.
The head of a rule is also called the goal and the predicates of the rule body are called
subgoals.
We will sometimes use a shorthand notation for Datalog rules as follows:
\begin{align*}
Txy &\coloneq Exy \\
Txy &\coloneq Exy, Txy
\end{align*}
We call the predicates whose extent is completely specified to us as the extensional database
or the EDB.
The predicates whose extent is computed by the Datalog rules, are called the intensional database
or the IDB.
One of the IDB predicates is designated to be the \emph{output} predicate.
A Datalog program, therefore, defines a mapping from the EDB to IDB.

\subsection{Operational Semantics of Datalog}
\label{sec:LC1:OpSem}

In this section, we define the meaning (or, semantics) of the Datalog rules.
For illustration, let $P$ be the following Datalog program: 
\begin{align*}
Txy &\coloneq Exy \\
Txy &\coloneq Exy, Txy
\end{align*}
Here, $E$ is the EDB predicate and $T$ is the IDB predicate.
Let $D$ be the active domain of the EDB.
Therefore, $E \subseteq D \times D$. Similarly, $T \subseteq D \times D$.
If we denote the power set of $D \times D$ as $2^{D \times D}$, then the semantics of 
program $P$ is $P$ : $2^{D \times D} \to 2^{D \times D}$.

We give an operational meaning to these semantics as follows:
\begin{enumerate}[noitemsep, topsep=0pt, label={}]
\item $ T = \emptyset $
\item $ repeat $
   \begin{enumerate}[noitemsep, topsep=0pt, label={}]
   \item $ S = T $
   \item $ T = E \cup E;T $
   \end{enumerate}
\item $ until \; S == T $
\end{enumerate}
Note: The semicolon operator `;' used above is defined thus:
\begin{defn}[Operator `;'] \label{def:LC1:OpSemi}
If $R_1 \subseteq A \times B$ and $R_2 \subseteq B \times C$, then $R_1;R_2 \subseteq A \times C$ where
$R_1;R_2 = \{(x,z) \mid x \in A \wedge z \in C \wedge \exists y \in B \; s.t.\; ((x,y) \in R_1 \wedge (y,z) \in R_2)\}$
\end{defn}

The operational semantics given above is implementable.
But in order to compare the efficiency of different implementations, we need a semantics that is independent
of the implementation.
For this purpose, we define the denotational semantics of Datalog.

\subsection{Denotational Semantics of Datalog}
\label{sec:LC1:DeSem}

The denotational semantics of Datalog is the \emph{least fixed point} semantics.
We start by understanding the word \emph{least} in the context of Datalog semantics.
By \emph{least} we mean the smallest extent of the IDB predicates such that the Datalog
rules cannot derive any more new facts.
To specify the smallest extent, the set of all possible values for the extent of IDB relations
must be ordered.
\begin{defn}[Partial Order] \label{def:LC1:POrder}
A relation $\leq$ on a set $A$, denoted $(A, \leq)$, is a partial order if the relation $\leq$
is reflexive, antisymmetric and transitive.
\end{defn}
Note that the relation $\leq$ is reflexive if $\forall a \in A, a \leq a$.
It is antisymmetric if $\forall a,b \in A, a \leq b \wedge b \leq a \implies a = b$.
It is transitive if $\forall a,b,c \in A, a \leq b \wedge b \leq c \implies a \leq c$.
Examples of partial orders are:
\begin{enumerate}
\item $(2^{D \times D}, \subseteq)$,
\item $({\mathbb N}, \mid)$ (Note: $x \mid y$ means that $x$ divides $y$ exactly), and
\item $(Interval({\mathbb R}), \leq)$ where the interval $[x,y] \leq [z,u]$ iff $x \leq z$ and $y \leq u$.
\end{enumerate}
A partial order $(A, \leq)$ is a total order if $\forall a,b \in A$, either $a \leq b$ or $b \leq a$.


\begin{defn}[Upper Bound] \label{def:LC1:UBound}
Let $(A, \leq)$ be a partial order and let $S \subseteq A$.
Then, $u \in A$ is an upper bound of $S$ if $\forall s \in S, s \leq u$.
\end{defn}

\begin{defn}[Least Upper Bound] \label{def:LC1:LUB}
Let $(A, \leq)$ be a partial order and let $S \subseteq A$. $w$ is the least upper bound of $S$ if:
\begin{inparaenum}[(\itshape a\upshape)]
\item $w$ is an upper bound, and
\item $\forall x \in A, x$ is an upper bound $\implies w \leq x$.
\end{inparaenum}
\end{defn}

\begin{defn}[Lower Bound] \label{def:LC1:LBound}
Let $(A, \leq)$ be a partial order and let $S \subseteq A$. Then, $u \in A$ is a lower bound of $S$ if $\forall s \in S, u \leq s$.
\end{defn}

\begin{defn}[Greatest Lower Bound] \label{def:LC1:GLB}
Let $(A, \leq)$ be a partial order and let $S \subseteq A$. $w$ is the greatest lower bound of $S$ if:
\begin{inparaenum}[(\itshape a\upshape)]
\item $w$ is a lower bound, and
\item $\forall x \in A, x$ is a lower bound $\implies x \leq w$.
\end{inparaenum}
\end{defn}

A least upper bound is called \emph{lub} or \emph{sup}. It might not always exist for all subsets.
Examples: The partial order $(2^{D \times D}, \subseteq)$ has least upper bounds for all subsets of $2^{D \times D}$ 
whereas the partial orders $({\mathbb N}, \mid)$ and $(Interval({\mathbb R}), \leq)$ do not have \emph{lub}s for
all subsets.
Similarly, a greatest lower bound is called \emph{glb} or \emph{inf}. It might not always exist for all subsets.

\begin{defn}[Complete Lattice] \label{def:LC1:CLatt}
A partial order $(A, \leq)$ is a complete lattice if $\forall S \subseteq A, sup(S)$ and $inf(S)$ exist.
\end{defn}

\begin{defn}[Monotone Function (over partial orders)] \label{def:LC1:MonF}
Let $(A, \leq)$ and $(B, \leq)$ be two partial orders and let $f : A \to B$.
Function $f$ is monotone if $\forall x,y \in A, x \leq y \implies f(x) \leq f(y)$.
\end{defn}
