\section{Lecture: Thursday, September 6th}
Note taker: Sulekha Kulkarni 
\label{sec:LC3}

\subsection{Kleene Fixpoint Theorem}
\label{sec:LC3:Kleene}

Before we state and prove Kleene's fixpoint theorem, we need a few definitions and a lemma.

\begin{defn}[$\omega$-chain] \label{def:LC3:OChain}
Let $(L, \leq)$ be a partial order. Then an $\omega$-chain is a sequence $a_0 \leq a_1 \leq a_2 \leq \ldots$
where each $a_i \; i \in {\mathbb N}, \in L$. 
\end{defn}

It is called an $\omega$-chain because the indices $0, 1, 2, \ldots$ are natural numbers.
Traditionally, $\omega$ is the set of natural numbers.
It could have as well been called ${\mathbb N}$-chain.

\begin{defn}[$\omega$-continuity] \label{def:LC3:OCont}
Let $(L, \leq)$ be a complete lattice. A function $f : L \rightarrow L$ is $\omega$-continuous if for every $\omega$-chain
$a_0 \leq a_1 \leq a_2 \leq \ldots$ in $(L, \leq)$, $f(sup_{n\geq 0}(a_n)) = sup_{n\geq 0}(f(a_n))$.
\end{defn}

We will use the following convenience notation hereafter: $f(f(x))$ will be denoted as $f^2(x)$, $f(f(f(x)))$ as
$f^3(x)$ and so on.

\begin{lemma} \label{lemma:LC3:OC}
Let ${\bot}$ be the least element of a lattice $(L, \leq)$. Then, the sequence $\bot, f(\bot), f^2(\bot), f^3(\bot), \ldots$
forms an $\omega$-chain.
\end{lemma}

\begin{proof}
We prove by induction on the length of the sequence. The base case holds obviously since the since the first
element of the sequence, ${\bot} = \emptyset$, is $\leq$ any element of $L$. We assume that the lemma holds for
sequences of length $k$. That is, let the inductive hypothesis (IH) be $f^k(\bot) \leq f^{k+1}(\bot)$.
We want to show (WTS) that $f^{k+1}(\bot) \leq f^{k+2}(\bot)$. We apply $f$ to both sides of the IH and the
resulting relation $\leq$ holds true because $f$ is monotone. 
\end{proof}

\begin{theorem}[Kleene Fixpoint]
Let $(L, \leq)$ be a complete lattice and $f : L \rightarrow L$ be a $\omega$-continuous, monotone function.
Then $f$ has a least fixpoint $p$ where $p$ is the supremum of the $\omega$-chain
$\bot, f(\bot), f^2(\bot), f^3(\bot), \ldots$. That is, $p = sup_{n\geq 0}(f^n(\bot))$.
\end{theorem}

\begin{proof}
Let $p = sup_{n\geq 0}(f^n(\bot))$. We need to show that $p$ is the least fixpoint of $f$. The lemmas~\ref{lemma:LC3:KF2}
and~\ref{lemma:LC3:KF3} below establish that $p$ is a fixpoint by showing $p = f(p)$. That is, they respectively show
that $p \leq f(p)$ and $f(p) \leq p$. Lemma~\ref{lemma:LC3:KF5} shows that $p$ is the \emph{least} fixpoint of $f$.
\end{proof}

\begin{lemma} \label{lemma:LC3:KF1}
$\forall_{n \in {\mathbb N}}\; f^n(\bot) \leq f(p)$.
\end{lemma}

\begin{proof}
There are two cases for $n$.
\begin{inparaenum}[(\itshape a\upshape)]
\item $n = 0$: the claim trivially holds because $\bot$ is less than any element of $L$.
\item $n > 0$: let $n = k+1$. We want to show that $f^{k+1}(\bot) \leq f(p)$. Since $f$ is monotone, it is
enough to show $f^{k}(\bot) \leq p$. But this holds because $p$ is the supremum of all $f^n(\bot)$.
\end{inparaenum}
\end{proof}

\begin{lemma} \label{lemma:LC3:KF2}
If $p = sup_{n\geq 0}(f^n(\bot))$, then $p \leq f(p)$.
\end{lemma}

\begin{proof}
Follows from lemma~\ref{lemma:LC3:KF1} and the fact that if all elements
of the set $\{f^n(\bot) \mid n \in {\mathbb N}\}$ are $\leq f(p)$, then the supremum of that set which
is $p$, is also $\leq f(p)$.
\end{proof}

\begin{lemma} \label{lemma:LC3:KF3}
If $p = sup_{n\geq 0}(f^n(\bot))$, then $f(p) \leq p$.
\end{lemma}

\begin{proof}
Substituting for $p$ in $f(p)$, we have $f(p) = f(sup_{n \geq 0}(f^n(\bot))$. Since $f$ is $\omega$-continuous,
$f(p) = sup_{n \geq 0}(f(f^n(\bot))) = sup_{n \geq 0}(f^{n+1}(\bot)) \leq sup_{n \geq 0}(f^n(\bot))$ which is equal to $p$.
Therefore, $f(p) \leq p$.
\end{proof}

\begin{lemma} \label{lemma:LC3:KF4}
Let $q$ be any fixpoint of $f$. Then, $\forall_{n \in {\mathbb N}}\; f^n(\bot) \leq q$.
\end{lemma}

\begin{proof}
The proof is by induction on $n$. The base case when $n = 0$ holds trivially because $\bot \leq q$.
For the inductive case when $n > 0$, assume the IH $f^k(\bot) \leq q$ holds. We want to show that
$f^{k+1}(\bot) \leq q$. Applying $f$ to both sides of the IH,
$f^{k+1}(\bot) \leq f(q)$. Since $q$ is a fixpoint, $f(q) = q$. Therefore, $f^{k+1}(\bot) \leq q$.
\end{proof}

\begin{lemma} \label{lemma:LC3:KF5}
Let $q$ be another fixpoint of $f$. That is, $q = f(q)$. Then, $p \leq q$.
\end{lemma}

\begin{proof}
By lemma~\ref{lemma:LC3:KF4}, we have $\forall_{n \in {\mathbb N}}\; f^n(\bot) \leq q$. If all elements
of the set $\{f^n(\bot) \mid n \in {\mathbb N}\}$ are $\leq q$, then the supremum of that set which
is equal to $p$, is also $\leq q$.
\end{proof}

\subsection{A Closer Look at Transitive Closure - continued}
\label{sec:LC3:TransClos}
In Section~\ref{sec:LC2:TransClos}, we left the proof of $lfp(g) \subseteq lfp(f)$ for later.
We will now complete this proof.

\begin{claim}
Let $R = \cup_{n \geq 0} f^n(\emptyset)$. Then, $R = \cup_{k \geq 0} E^k$.
\end{claim}

\begin{lemma} \label{lemma:LC3:TC1}
$lfp(g) \subseteq lfp(f)$.
\end{lemma}

\begin{proof}
Since $lfp(g) = \cup_{n \in {\mathbb N}}(g^n(\emptyset))$, we prove that
$\cup_{n \in {\mathbb N}}(g^n(\emptyset)) \subseteq lfp(f)$ by induction on $n$.
The base case where $n = 0$ is trivially true because $\emptyset \subseteq lfp(f)$.
For the inductive case of $n > 0$, assume the IH $g^k(\emptyset) \subseteq lfp(f)$.
We need to show that $g^{k+1}(\emptyset) \subseteq lfp(f)$.

For this proof, we will adopt the following convenience notation:
\begin{itemize}
\item $R$ denotes $lfp(f)$
\item $T_0$ denotes $\emptyset$
\item $T_1$ denotes $g(T_0)$ which is equal to $E \cup T_0;T_0$ \\ $\vdots$
\item $T_{k+1}$ denotes $g(T_k)$ which is equal to $E \cup T_k;T_k$
\end{itemize}

With this notation, the IH is $T_k \subseteq R$. We need to show that $T_{k+1} \subseteq R$.
$T_{k+1} = g(T_k) = E \cup T_k;T_k \subseteq E \cup R;R$.

We use the claim above to prove that $R;R = R$.
$R;R = (\cup_{i \geq 0}E^i);(\cup_{j \geq 0}E^j) = \cup_{i \geq 0}(E^i;(\cup_{j \geq 0}E^j))$
because composition distributes over union by proposition~\ref{prop:LC2:ComDistU}.
This is equal to $\cup_{i \geq 0}\;\cup_{j \geq 0} (E^i;E^j)$. This is the same as $\cup_{k \geq 0} E^k = R$.

Therefore, $T_{k+1} \subseteq E \cup R$. Since $R$ is $lfp(f)$, $E \cup R = R$. Therefore, $T_{k+1} \subseteq R$.
\end{proof}
