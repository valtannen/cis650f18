\section{Lecture: Thursday, November 29th}
Note taker: Pardis Pashakhanloo
\label{sec:LC23}

\subsection{Integrity Constraints (Dependencies)}
Let us start with an example. In table Employee below, we cannot have (0001, 3). We might also have additional constraints.

\begin{table}[H]
	\centering
\begin{tabular}{|c|c|}
	\hline
	SSN&Dept\\
	\hline
	0001 & 2
\end{tabular}
\quad
\begin{tabular}{|c|c|}
	\hline
	DID&Mgr\\
	\hline
	2 & 0001
\end{tabular}
\caption{``Employee" table at left, and ``Dept" table at right.}
\end{table}
The concept of integrity constraints was invented by Codd who is also the inventor of the relational database. One other example constraint is ``SSN determines Dept". This is called a \textbf{functional dependency (FD)}. A primary key is a special kind of functional dependency. The next kind of dependency is \textbf{inclusion dependency (ID)}. An example is the foreign key.\\
We can write these dependencies more formally as below:
\begin{align*}
\forall{s, d, d'} E_{s,d} \vee E_{s,d'} \implies d=d' \tag{FD}\\
\forall{s, d} E_{s,d} \implies \exists_{m} D_{d,m} \tag{ID}
\end{align*}

\begin{table}[H]
	\centering
	\caption{R}
\begin{tabular}{|c|c|c|}
	\hline
	A&B&C\\
	\hline
	\-&\-&\-
\end{tabular}
\end{table}

Does the following always hold?
\begin{align*}
\Pi_{AB}R ++  \Pi_{BC} R = R
\end{align*}

For answering this question, we have to look at two inclusions here:

\begin{align*}
\Pi_{AB}R ++ \Pi_{BC} R \subseteq R\\
\Pi_{AB}R ++ \Pi_{BC} R \subseteq R % use the reverse subseteq
\end{align*}

The second one is always true and we can demonstrate it using an example: Tuple $(a,b,c) \in R$, $(a,b) \in \Pi_{AB}R$, and $(b,c) \in \Pi_{BC}R$.

However, the first one does not always hold as illustrated below. We will get more tuples by projecting and joining. (In this example, abc' and a'bc.)

\begin{table}[H]
	\centering
	\caption{R}
	\begin{tabular}{|c|c|c|}
		\hline
		A&B&C\\
		\hline
		a&b&c\\
		a'&b&c'
	\end{tabular}
\end{table}

So we need to define a dependency here, called \textbf{join dependency (JD)}.
\begin{align*}
\forall{x,y,z,x',z'} R_{x,y,z}  R_{x,y,z} \wedge R_{x',y,z'} \implies R_{x,y,z'} \wedge R_{x',y,z} \tag{JD}
\end{align*}

Now, let's get back to conjunctive queries (CQ).
\begin{align*}
ans(y,z) :- R_{x,y,z'}, R_{x,y',z} \tag{q}
\end{align*}
In all databases, this query is considered minimal. But let's introduce some constraints next:
\begin{align*}
\forall{x,y_1,z_1,y_2,z_2} R_{x,y_1,z_1} \vee R_{x,y_2,z_2} \implies y_1=y_2 \tag{$\phi$}
\end{align*}
In all databases that satisfy $\phi$, we have the equivalence $q \equiv q'$ where:
\begin{align*}
ans(y,z) :- R_{x,y,z} \tag{q'}
\end{align*}

Under key constrains, we can do some simplifications. All good database engines exploit this optimization opportunity. The reason that it is very effective is eliminating join operations.
For instance, $ans(s,d) :- E_{s,d}, D_{d,m}$ is the same as $ans(s,d) :- E_{s,d}$.

Note that, while it is true that experienced programmers write manually optimized queries, it is not the case for all programmers.

\subsubsection{Conjunctive Containment Dependencies (CCD)}
\paragraph{Syntax.} $\forall{\bar{x}} ( B \implies \exists{\bar{ y } } C )$
\paragraph{Explanation.} $B$ is a conjunction of atoms (body of CQ), and $C$ is a conjunction of atoms plus equalities.

We can write the aforementioned dependencies in this format:
 \begin{align*}
\forall{\bar{x}}  (R_{\bar{x}_1} \wedge R_{\bar{x}_2}  \implies y_1 = y_2 )  \tag{FD}\\
\forall{s, d} (R_{s,d} \implies \exists_{m} D_{d,m} )  \tag{ID}\\
	\forall{\bar{x}} (R_{\bar{y}_1} \wedge R_{\bar{y}_2} \implies R_{\bar{z}_1} \wedge R_{\bar{z}_2} )  \tag{JD}
\end{align*}

Let us take a look at an example:
\begin{align*}
	ans(\bar{u}) :- B(\bar{u}, \bar{x}) \tag{q}\\
	ans(\bar{u}) :- B(\bar{u}, \bar{y}) \tag{q'}
\end{align*}

For $cont(q, q')$ we want to write a CCD, where $cont$ is short for containment.
\begin{align*}
\forall{\bar{u}} \forall{\bar{x}} B(\bar{u}, \bar{x}) \implies \exists{\bar{y}} C(\bar{u}, \bar{y})
\end{align*}
We claim that,
\begin{align*}
\forall_{I} I \vDash cont(q,q') \iff q(I) \subseteq q'(I)
\end{align*}
where $I$ is an instance of the database.

\begin{align*}
	\forall_{\bar{u}} ( \exists{\bar{x}} B(\bar{u}, \bar{x}) ) \implies ( \exists{\bar{y}} B(\bar{u}, \bar{y}) )
\end{align*}
where the left-hand side is the output of $q$ and the right-hand side is the output of $q'$.

\subsection{Chase}
Given $\forall{\bar{x}} (B \implies \exists \bar{y} C)$, and CQ $ans(\bar{u}) :- T$, we can apply Chase to $T$, and in the process, we will produce a new CQ $ans(\bar{u}) :- T'$, when:
\begin{enumerate}
\item There  exists a homomorphism $h : B \rightarrow T$.
\item $h$ cannot be extended to a homomorphism $h : B \union C \rightarrow T$.
\end{enumerate}
When (1) and (2) hold, we say Chase is applicable, and:
\begin{align*}
T' = T \union C [ \bar{x} \coloneqq  h(\bar{x})  ]
\end{align*}
This is called one step of Chase.

\paragraph{Example 1.}
\begin{align*}
&ans() :- R_{u_0 u_1}\\
&\forall{xy} R_{xy} \implies \exists{z} S_{zy} \tag{d1}\\
&h(x) = u_0, h(y) = u \implies ans() :- R_{u_0u_1}, S
\end{align*}

\paragraph{Example 2.}
\begin{align*}
	&ans() :- R_{u_0 u_1}\\
	&\forall{xy} R_{xy} \implies \exists{z} S_{zy} \tag{d1}\\
	&\forall{xy} S_{xy} \implies \exists{z} R_{yz} \tag{d2}\\
\end{align*}
\paragraph{  Step 1:}
\begin{align*}
	&h(x) = u_0, h(y) = u_1 \\
	&ans() :- R_{u_0u_1}, S{z_0u_1}
\end{align*}

\paragraph{  Step 2:}
\begin{align*}
	&h(x) = z_0, h(y) = u_1 \\
	&ans() :- R_{u_0u_1}, S_{z_0y_1},R_{u_1z_1}.
\end{align*}

\paragraph{Termination of the chasing algorithm.}
In
$q \rightarrow_{d_1 \in D} q_1  \rightarrow_{d_2 \in D} \rightarrow ... \rightarrow q_n$
If $q_n$ is such that no more chasing is possible, we say that the algorithm terminates.

\paragraph{Theorem:} $D$ set CCD, and $q$ is a CQ. $q \rightarrow q_1  \rightarrow q_2 \rightarrow ... \rightarrow q_n$ is a terminating chase sequence with CCD's from $D$. Then,
$$
\forall{q'  \text{CQ } } D \vDash q \sqsubseteq q' \iff q_n \sqsubseteq q'.
$$

\begin{align*}
	& ans(y,z) :- R_{xyz'}, R{xy'z}\\
	& \forall{xy_1y_2z_1z_2} R_{xy_1z_1} \vee R_{xy_2z_2} \implies y_1 = y_2
\end{align*}

Here, the query can be minimized to $ans(y,z) :- R_{xyz}$.

\paragraph{Note 1.} The addition of atoms can help in minimization.
\paragraph{Note 2.} It is undecidable to prove the termination of chase. For guaranteeing termination, some work has been done.

