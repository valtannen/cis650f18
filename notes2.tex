\section{Lecture: Tuesday, September 4th}
Note taker: Pardis Pashakhanloo
\label{sec:LC2}

\subsection{ Knaster-Tarski Theorem  }
\label{sec:LC2:FP}
Before discussing Knaster-Tarski theorem, some propositions and definitions are needed.
\begin{prop}
In a complete lattice, for any $T \subseteq L$, $inf(T)$ exists.
\end{prop}
\begin{proof}
Let $T \subseteq L$. Consider $J = \{  x \in L | \forall y\in T , x \leq y \}$. $J$ is the set of the lower bounds of $T$. Since $J \subseteq L$, $sup(J)$ exists.
\end{proof}

\begin{claim}
$sup(J)$ is the $inf(T)$.
\end{claim}
\begin{proof}
We must show two things:
\begin{enumerate}
\item $sup(J)$ is a lower bound of $T$. For all $y\in T$, we need to show that $sup(J)\leq y$:
$\forall x\in J x\leq y \implies sup(J)\leq y$.

\item $sup(J)$ is bigger than any lower bound of $T$. This is trivial by definition.
\end{enumerate}
\end{proof}

\begin{prop}
Every complete lattice must have a least (minimum) element.
\end{prop}

\begin{defn}[Fixpoint] \label{def:LC2:FP}
Suppose $f:P\rightarrow P$. $x$ is a fixpoint of $f$ when $f(x)=x$. We denote the set of fixpoints of $f$ by $F_p(f)$.
\end{defn}

\begin{defn}[Pre-fixpoint] \label{def:LC2:PreFP}
	Suppose $f:P\rightarrow P$. $x$ is a pre-fixpoint of $f$ when $f(x)\leq x$. We denote the set of pre-fixpoints of $f$ by $Pre(f)$.
\end{defn}

\begin{defn}[Post-fixpoint] \label{def:LC2:PostFP}
	Suppose $f:P\rightarrow P$. $x$ is a post-fixpoint of $f$ when $x\leq f(x)$. We denote the set of post-fixpoints of $f$ by $Post(f)$.
\end{defn}

\begin{theorem}[Knaster-Tarski]
If $L$ is a complete lattice and
$f:L\rightarrow L$ is monotone, then $f$ has
a least fixpoint which is denoted by $lfp(f)$.
\end{theorem}
\begin{proof}
$p=inf(Pre(f))$.

\begin{claim}
$p$ is what we are looking for.
\end{claim}

We must show two things:
\begin{enumerate}
\item $f(p)\leq p$\\
It is enough to show $f(p)$ is a lower bound to $Pre(f)$. In other words, $\forall q\in Pre(f)$, 
we want to show $f(p)\leq q$.
Since $p=inf(Pre(f))$ and $q\in Pre(f)$, $p \leq q$ which implies that $f(p)\leq f(q)\leq q$.
\item $p\leq f(p)$\\
We know that $f(p)\in Pre(f)$. From the monotonicity of $f$ and the previous part, $f(f(p))\leq f(p)$.
\end{enumerate}
Therefore, $f(p)=p$.
\end{proof}

\subsection{ A Closer Look at Transitive Closure }
\label{sec:LC2:TransClos}

Both set of Datalog rules below compute the transitive closure. However, by comparing their operational semantics, we observe that $B$ is slower than $A$.

\begin{align*}
A \begin{cases}
Txy &\coloneq Exy \\
Txy &\coloneq Exz, Tzy
\end{cases}
\end{align*}
\begin{align*}
B \begin{cases}
Txy &\coloneq Exy \\
Txy &\coloneq Txz, Tzy
\end{cases}
\end{align*}

Let's take a look at the operational semantics of $A$ and $B$, respectively.
\begin{enumerate}[label=(\Alph*)]
\item
\begin{enumerate}[noitemsep, topsep=0pt, label={}]
	\item $ T = \emptyset $
	\item $ repeat $
	\begin{enumerate}[noitemsep, topsep=0pt, label={}]
		\item $ S = T $
		\item $ T = E \cup E;S $
	\end{enumerate}
	\item $ until \; S == T $
\end{enumerate}
\item
\begin{enumerate}[noitemsep, topsep=0pt, label={}]
	\item $ T = \emptyset $
	\item $ repeat $
	\begin{enumerate}[noitemsep, topsep=0pt, label={}]
		\item $ S = T $
		\item $ T = E \cup S;S $
	\end{enumerate}
	\item $ until \; S == T $
\end{enumerate}
\end{enumerate}

We observe that $f$ and $g$ do the same job. However, we need to prove it. In other words, we want to show that $lfp(f) = lfp(g)$.
\begin{align*}
&(2^{D\times D}, \subseteq)\\
&f : 2^{D\times D} \rightarrow 2^{D\times D} f(T)=E\cup E;T\\
&g : 2^{D\times D} \rightarrow 2^{D\times D} g(T)=E\cup T;T
\end{align*}

\begin{align*}
&f(T) \subseteq T\\
&E\cup E;T \subseteq T\\
&E \subseteq T\\
&E;T \subseteq T
\end{align*}
\begin{align*}
&g(T) \subseteq T\\
&E\cup T;T \subseteq T\\
&E \subseteq T\\
&T;T \subseteq T
\end{align*}

We know that $T;T \subseteq T$ implies $E;T\subseteq T$. Therefore, $Pre(g)\subseteq Pre(f)$.
From this, it follows that $lfp(f) \subseteq lfp(g)$.
To prove that $lfp(f) = lfp(g)$, we need to show that $lfp(g) \subseteq lfp(f)$.
We cannot proceed with this proof using Knaster-Tarski theorem.
We need a more powerful theorem, Kleene fixpoint theorem, that will be introduced in Section~\ref{sec:LC3:Kleene}
followed by a continuation of the proof in Section~\ref{sec:LC3:TransClos}.

\begin{lemma}
Binary relation composition is monotone in each argument. In other words,
$R_1 \subseteq R_2 \implies R_1;R_3\subseteq R_2;R_3 \implies R_4;R_1 \subseteq R_4;R_2$. (exercise: prove.)
\end{lemma}
\begin{cor}
$f$ and $g$ are monotone.
\end{cor}

\begin{prop} \label{prop:LC2:ComDistU}
	Composition distributes over union. In other words, $(A\cup B);C=(A;C)\cup (B;C)$.
\end{prop}

In order to see why method B is slower, we should compare the steps in A and B. Note that Proposition \ref{prop:LC2:ComDistU} is used for obtaining some steps.
\begin{align*}
A \begin{cases}
&\emptyset\\
&E\\
&E\cup E;E = E \cup E^2\\
&E\cup E;(E\cup E;E) = E \cup E^2 \cup E^3\\
& \ldots
\end{cases}
\end{align*}

\begin{align*}
B \begin{cases}
&\emptyset\\
&E\\
&E \cup E^2\\
&E\cup (E\cup E^2);(E\cup E^2)=E\cup E^2 \cup E^3 \cup E^3 \cup E^4\\
& \ldots
\end{cases}
\end{align*}

As you can see, even though they both seem to go the same, they compute different things at different steps. Since B performs some extra computations, it is generally slower. (Look at $E^3 \cup E^3$)

Kleene fixpoint theorem introduces another way of computing the least fixpoint that overcomes the limitations of Knaster-Tarski theorem.
To be continued in the next session.
