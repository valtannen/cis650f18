\section{Lecture: Tuesday, October 2nd}
Note taker: Hui Lyu
\label{sec:LC9}

\subsection{Review}
Datalog queries can be evaluated bottom-up or top-down.
For example, semi-naive evaluation is the simplest practical bottom-up technique, and SLD-resolution is a top-down technique.

Sometimes, we may not need to compute all derivations to answer a particular query. So a goal-directed approach is necessary.

\subsection{QSQ (Query-Subquery)}
\textit{QSQ (Query-Subquery)} is a technique for organizing top-down Datalog query evaluation.

The basic elements of QSQ framework are (refer to an \href{https://iccl.inf.tu-dresden.de/w/images/c/cc/DBT2016-Lecture-12.pdf}{\underline{online note}}):

\begin{itemize}
\item Apply \textbf{SLD-resolution}: start with query, find rules that can derive query, evaluate body atoms of those rules (subqueries) recursively
\item Evaluate intermediate results \textbf{set-at-a-time} (using relational algebra on tables)
\item Evaluate queries in a \textbf{data-driven} way, where operations are applied only to newly computed intermediate results
\item \textbf{Push} variable bindings (constants) from heads (queries) into bodies (subqueries)
\item \textbf{Pass} variable bindings (constants) \textbf{sideways} from one body atom to the next
\end{itemize}

\subsection{Example}
Suppose in a database we have three binary extensional predicates (EDB): F (Flat), U(Up), D(Down). The visualization is a labeled directed graph as follows:
\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{images/FUD-graph.pdf}
\caption{Input labeled directed graph}
\label{fig:FUD}
\end{figure}
So we have the following input tuples (EDB):

\begin{table}[H]
\centering
\begin{tabular}{lllll}
\multicolumn{1}{c}{\textbf{F}} & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{\textbf{U}} & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{\textbf{D}} \\ \cline{1-1} \cline{3-3} \cline{5-5} 
\multicolumn{1}{|l|}{m n}      & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{f m}       & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{m f}       \\ \cline{1-1} \cline{3-3} \cline{5-5} 
\multicolumn{1}{|l|}{m o}      & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{g n}       & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{i d}       \\ \cline{1-1} \cline{3-3} \cline{5-5} 
\multicolumn{1}{|l|}{g f}      & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{i o}       & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{g b}       \\ \cline{1-1} \cline{3-3} \cline{5-5} 
                               & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{a f}       &                       &                                \\ \cline{3-3}
\end{tabular}
\end{table}

Consider the following Datalog program where $R$ is the IDB predicate:
\begin{align*}
Rxy &\coloneq Fxy & (1) \\
Rxy &\coloneq Uxx_1, Ry_1x_1, Dy_1y & (2) \\
\end{align*}

We want to query:
\begin{align*}
query(v) &\coloneq Rav
\end{align*}

A semi-naive evaluation on $R$ is illustrated as follows:
\begin{table}[H]
\centering
\begin{tabular}{ll}
\multicolumn{2}{c}{\textbf{R}}                  \\ \hline
\multicolumn{1}{|l}{m} & \multicolumn{1}{l|}{n} \\
\multicolumn{1}{|l}{m} & \multicolumn{1}{l|}{o} \\
\multicolumn{1}{|l}{g} & \multicolumn{1}{l|}{f} \\ \hline
\multicolumn{1}{|l}{i} & \multicolumn{1}{l|}{f} \\
\multicolumn{1}{|l}{a} & \multicolumn{1}{l|}{b} \\ \hline
\multicolumn{1}{|l}{a} & \multicolumn{1}{l|}{d} \\ \hline
\end{tabular}
\end{table}
The first three tuples are derived using the first rule in the first round; the next two tuples are derived in the second round; and the last tuple is derived in the third round. Then the evaluation reaches to a fix point without any newly derived tuple. The answer to the query is $query(b), query(d)$.

\subsubsection{Adornments}
QSQ evaluation uses adorned predicates by adding superscripts to predicate names. The superscripts are whether $b$ (bound) or $f$ (free), meaning that the variable is bound or free. For example, in the query $Rav$, the variable $x$ is bound to constant $a$, and the variable $y$ is free. With adornment, we can rewrite the second rule as follows:
\begin{align*}
R^{bf}xy &\coloneq U^{bf}xx_1, R^{fb}y_1x_1, D^{bf}y_1y
\end{align*}
It is an ordered join in some certain way.

\subsubsection{Sideways Information Passing and Subqueries}
We firstly unify head with $Rav$.
\begin{enumerate}
\item subquery: ($U^{bf}xx_1, I x\boxed{a}$) \\
The first part is an adorned atom, and the second part is the set $I$ with element $x$. The value in the box denotes any value allowed to take for the element $x$. In this case, the value of $x$ is $a$. \\
The answer of the subquery is denoted as $J = \{x_1 | \exists x, Ix \wedge Uxx_1 \}$, where the relational algebra expression is $J= \Pi x_1 (I \bowtie U)$ \\
Then continue sideways information passing from atom to atom.
\item subquery: ($R^{fb}y_1x_1, J x_1\boxed{f}$) \\
The value of $x_1$ in $J$ is $f$ based on previous calculation. In terms of $J$, we can denote $K = \{y_1 | \exists x_1, Jx_1 \wedge Ry_1x_1 \}$, where the relational algebra expression is $K= \Pi y_1 (J \bowtie R^{prev})$. $R^{prev}$ includes three tuples: $(m,n), (m,o), (g,f)$.
\item subquery: ($D^{bf}y_1y, K y_1\boxed{g}$) \\
The value of $y_1$ in $K$ is $g$ based on previous calculation. In terms of $K$, we can denote $L = \{y | \exists y_1, Ky_1 \wedge Dy_1y \}$, where the relational algebra expression is $L = \Pi y (K \bowtie D)$. \\
The value of $y$ in $L$ is $b$ based on previous calculation $L y\boxed{b}$. Thus, we get one final answer of the query: $query(b)$.
\end{enumerate}
Likely, we can have other values in the previous boxes and derive other answers for the query. Finally we will get $query(b), query(d)$.

The equivalent rewritten rule is 
\begin{align*}
R^{bf}xy &\coloneq Ix, U^{bf}xx_1, Jx_1, R^{fb}y_1x_1, Ky_1, D^{bf}y_1y, Ly
\end{align*}
which is an iterative program of relational algebra.

\subsubsection{Optimization}
When rewriting a rule with adornment, we can reordering the rule body predicates for optimization purpose. For example, we can rewrite the rule as 
\begin{align*}
R^{fb}xy &\coloneq D^{fb}y_1y, R^{bf}y_1x_1, U^{fb}xx_1
\end{align*}
Since $y$ in rule head is bound, we move predicate $D$ which has variable $y$ to the first rule body predicate, to reduce derived tuples.