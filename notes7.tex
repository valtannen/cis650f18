\section{Lecture: Thursday, September 25th}
Note taker: Cyril Saade
\label{sec:LC1}

\subsection{Review}

Last time, we discussed Model-Theoretic Semantics for Datalog.

\begin{align*}
Txy &\coloneq Exy \\
Txy &\coloneq Exz, Tzy \\
Ry &\coloneq Txy
\end{align*}

The active domain is thus: $$D=\{a,b,c,d\}$$ Therefore, we know that the following is true: $Eab$, $Ebc$, $Ecc$, $Ecd$. Converting each rules in a first order sentence will give us:

$$ \forall xy, Exy \to Txy $$
$$ \forall xyz, Exz \land Tzy \to Tzy$$
$$ \forall xy, Txy \land Exx \to Ry$$

A conjunction of these first order models is $\phi_p$. \newline \newline As discussed in the previous lectures, there are two types of proof theoretic semantics:
\begin{enumerate}
\item Proof Trees (covered in previous lectures)
\item Single Definite Sense (SLD)-Resolution 
\end{enumerate}
In this session, we will discuss SLD-Resolution as a different proof theoretic semantic. SLD are expressed in terms of refutations (i.e. elements we understand).

\subsection{Set Theoretic Semantics}

In order to formulate a refutation, we need $\phi_p$ and a first order ground theory (FO-Theory); the FO-groupd theory of $P$. \newline

First, we take the instantiations of the rules (i.e. all possible group sentences). From the transitive closure example given above, we obtain:

$$ \Phi_p = \{ Eab, Eab\to Tab, Eab \land Tbc \to Tac, Tab\to Rb,... \} $$

We can recall that all these instatiations were used to satisfy the premices in a proof tree (i.e. Modus Ponens).\newline We can further suggest that: \newline

$\mathcal{A} \models \phi_p \implies \mathcal{A} \models \Phi_p$ (this is true for any model)\newline

$\mathcal{A} \models \phi_p \impliedby \mathcal{A} \models \Phi_p$ (this is true in which the universe of $\mathcal{A}$ is the active domain $D$).\newline

We can now describe the set theoretic semantics as follows:
\begin{prop}
For a datalog program $P$, recall the fixpoint semantics: $\union_{i=0}^{\inf}f^i(\emptyset) = \{(u,v) \in D\times D | \Phi_p \models Tuv\}$. We propose that this is equal to $\{(u,v) \in D\times D | \mathcal{A}\models Tuv\} = \{(u,v) \in D\times D | \phi_p \models Tuv\}$
\end{prop}

\textbf{Notation explanation}: what does it mean for a set of sentences to have a set as a logical consequences? This occurs when: $\Phi \models \phi$ and is true when: $\forall \mathcal{A}$ if $\mathcal{A} \models \Phi$ then $\mathcal{A}\models \Phi$.

\subsection{Refutations: Ground Refutations}

In this class, we will learn two types of refutations; we will now start by describing ground refutations. 

In fact, if our objective is to prove $\phi$ then we could accomplish this by doing a proof by contradiction. 

In other words, we should assume that $\neg \phi$ is true until we reach a contradiction $\Psi \land{\Psi}$ (which is obviously false). Ultimately, this will prove that $\neg \phi$ is false; which will prove that $\phi$ is true.\newline

Therefore, proving that an extensional fact is true can be achieve by assuming it is not until we reach a contradiction. Furthermore, in a ground refutation, only the datalog program $P$'s ground facts are used in order to reach the contradiction. We will now show an example (based on the transitive closure).

\begin{align*}
\neg R_d &\implies \neg T_{ad} (T_{ad}\to R_d \in \Phi) \\
\neg T_{ad} &\implies \neg E_{ab} \lor \neg T_{bd} (E_{ab}\land T_{bd} \to T_{ad} \in \Phi_P) \\
\neg E_{ab} \lor \neg T_{bd} &\implies False \lor \neg E_{bc} \lor \neg T_{cb} (E_{ab}\to True, E_{bc}\land T_{cd}\to T_{bd} \in \Phi_P) \\
\neg E_{bc} \lor \neg T_{cb} &\implies False \lor \neg E_{cd} (E_{bc}\to True, E_{cd}\to T_{cd}) \\
\neg E_{cd} &\implies False (E_{cd}\to True)
\end{align*}

Therefore, this shows that $\neg R_d$ is false. Therefore, $R_d$ is implied by all instantiations of $\Phi_P$ of the Datalog program $P$. The proof tree is illustrated below.

\begin{center}
\begin{tikzpicture}[grow'=up,scale=0.75]
\Tree [.$\fbox{false}$
[ . \node(n1){$true\rightarrow E_{cd}$};]
[ . \node(n2){$\fbox{false} \vee \neg E_{cd}$};
[. \node(n7){$true\rightarrow E_{bc}$};]
[ . \node(n3){$\fbox{false}\vee \neg E_{bc} \vee \neg T_{cd}$};
[. \node(n5){$true\rightarrow E_{ab}$};]
[. \node(n6){$\neg E_{ab} \vee \neg T_{bd}$};
[. \node(n9){$\neg T_{ad}$};
[. \node(n11){$\neg R_d$};]
[. \node(n12){$T_{ad}\rightarrow R_d$};]
]
[. \node(n10){$E_{ab} \wedge T_{bd} \rightarrow T_{ad}$};]
]
[. \node(n8){$E_{bc} \wedge T_{cd} \rightarrow T_{bd}$};]		
]
[. \node(n4){$E_{cd}\rightarrow T_{cd}$};]
]
]
]
\end{tikzpicture}
\end{center}

\begin {prop}
If there exists a ground refutation for $R_d$, then $\Phi_P\models R_d$.
\end{prop}

We would now want to show the completeness of SLD-Resolutions.

\begin{prop}
If $\Phi_P \models R_d$, then there exists a ground refutation for $R_d$.
\end{prop}
Note: $\Phi_P\models R_d$ means that there exists a proof tree (and that it's in the output of the program). Therefore, turning the proof tree around will produce a ground refutation.


\subsection{Refutations: Resolutions}

As a second flavor of refutations, resolutions represent a more subtle way of reaching a refutation. In fact, instead of working directly with ground instantiations, we will work with rules of the Datalog program. We now provide an example of a resolution (based on the transitive closure rules formulated above).

In this example, we will want to prove $R_y$. Therefore, we will assume $\neg R_y$ and attempt to find a $y$ for which this assumption does not hold.
\begin{align*}
\neg R_y &\implies \neg E_(xx) \lor \neg T_{xy} (\forall xy', T_{xy'}\land E_{xx})\\
\neg E_(xx) \lor \neg T_{xy} &\implies False \lor \neg T_{cy} (x\to c, E_{cc}\to True)\\
\neg T_{cy} &\implies \neg E_{cy} (\forall{x'y''} E_{x'y''}\to T_{x'y''})\\
\neg E_{cy} &\implies False (y\to c, y\to d)
\end{align*}
Therefore, we have just proven that $\neg R_y$ is false for $y=c$ and $y=d$. Thus, this shows that $R_d$ is true, by resolution.

