\section{Lecture: Tuesday, November 13th}
Note taker: Chris Cahill
\label{sec:LC19}

\subsection{Bag Semantics in Datalog}

We will continue using bag semantics in Datalog for aggregations: count, sum (and thereby, avg), as well as min, max. \newline 

Bag Semantics 

\begin{enumerate}
\item Contains the multiplicity of every element
\item For sets with $D$ (the domain of values), $D \mapsto \mathbb{B}$ (boolean)
\item For bags with $D$ (the domain of values), $D \mapsto \mathbb{N}$ (natural number)
\end{enumerate}

However, sometimes in Datalog, on top of $\mathbb{N}$, the concept of infinity is also needed. \newline 

Infinity with Fixed Points
\begin{enumerate}
\item $D \mapsto \mathbb{N}^{\infty}$ for sups of $\omega$-chains 
\item $\infty$ is greater than all natural numbers in $\mathbb{N}$ 
\item $\infty = \infty + \infty$ 
\end{enumerate}

In Bags+, the multiplicity is in $\mathbb{N}^{\infty}$, not just $\mathbb{N}$. Thus, in Bags+, $D \mapsto \mathbb{N}^{\infty}$. We will also define $sup(B) = \{d \in D | B(d)$ $!= 0\}$. If finite, we can apply Knaster-Tarski and Kleene. \newline  

\begin{align*}
Txy &\coloneq Exy\\
Txy &\coloneq Exz, Tzy\\
\end{align*}

\begin{figure}[t]
\begin{center}
        \includegraphics[scale=0.6]{images/sample_graph} \\
\end{center}
\hrule width \hsize height .33pt
\vspace{0.03in}
\caption{Example graph}
\label{fig:LC4:gr}
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{lllll}
\multicolumn{1}{c}{\textbf{E}} \\ \cline{1-1}
\multicolumn{1}{|l|}{a b}    \\ 
\multicolumn{1}{|l|}{a c}    \\ 
\multicolumn{1}{|l|}{b c}    \\
\multicolumn{1}{|l|}{c d}    \\ 
\multicolumn{1}{|l|}{d b}    \\  \cline{1-1}
\end{tabular}
\end{table}

To start, 
\begin{enumerate}
\item $T = \emptyset$
\item Iterate with original rule $T = E \cup E;T$
\end{enumerate}

\begin{table}[H]
\centering
\begin{tabular}{lllll}
\multicolumn{1}{c}{\textbf{T}} \\ \cline{1-1}
\multicolumn{1}{|l|}{E}    \\ 
\multicolumn{1}{|l|}{-----}    \\ 
\multicolumn{1}{|l|}{a c 2}    \\ 
\multicolumn{1}{|l|}{a d 2}    \\
\multicolumn{1}{|l|}{b d 2}    \\ 
\multicolumn{1}{|l|}{-----}    \\ 
\multicolumn{1}{|l|}{a d 3}    \\  \cline{1-1}
\end{tabular}
\end{table}

Eventually, there will be infinite paths from $a \rightarrow d$. Thus, bottom-up/naive/semi-naive evaluation will not work as before. Thus, the problem: we cannot use standard Datalog on Bags+. However, we can replace values with $\infty$ after a certain point in time, if clever. Once $\infty$ is noted as a value, all of the rules of old Datalog can be used. \newline 

Before, we came across proof-theoretic semantics:
\begin{enumerate}
\item Derivation trees 
\item SLD resolutions, proofs by refutation 
\end{enumerate}

Derivation Trees:
\begin{enumerate}
\item D active domain 
\item Rule instantiations (rule grounding) \newline 
\end{enumerate}

Proof Trees: 
\begin{align*}
Tab &\coloneq Eab\\
Tac &\coloneq Eab, Tbc\\
\end{align*}

The same fact can have multiple derivations. There may be infinitely many trees that produce T_{ad}. 

\begin{displaymath}
\prftree[r]{\textit{}}
  {Eab}
  {\prftree{Ebd}{Tbd}}
{Tad}
\end{displaymath}

--

\begin{displaymath}
\prftree[r]{\textit{}}
  {Eab}
  {\prftree{Ebc}{\prftree{Ecd}{Tcd}}{Tbd}}
{Tad}
\end{displaymath}

With infinity, can we extend semi-naive algorithm so it works in finite time? \newline 

Yes, there is an established test that detects when # of derivation trees will be infinite. 

\begin{figure}[t]
\begin{center}
        \includegraphics[scale=0.4]{images/firsttree} \\
        \includegraphics[scale=0.4]{images/replacetree} \\
\end{center}
\hrule width \hsize height .33pt
\vspace{0.03in}
\caption{When F is found in its own tree, replace and repeat}
\label{fig:LC4:gr}
\end{figure}

In other words, if when traversing the tree of F, we encounter F again, we can therefore take out the chunk about the found F and replace it with the current tree, thereby making it infinite. If this situation above occurs, it means infinity-many trees may occur. From Mumick and Shimeli, \newline 

$T$ starts off as a copy of $E$, but now we maintain two extra pieces of information: 
\begin{enumerate}
\item multiplicity (which starts off as 1) 
\item a set of ancestor facts
\end{enumerate}

For example, 

\begin{table}[H]
\centering
\begin{tabular}{lllll}
\multicolumn{1}{c}{\textbf{T}} \\ \cline{1-1}
\multicolumn{1}{|l|}{Nodes $\vert$ Mult. $\vert$ Ances.}    \\ 
\multicolumn{1}{|l|}{a b  $\vert$ 1 $\vert$ $\emptyset$}    \\ 
\multicolumn{1}{|l|}{a c  $\vert$ 1 $\vert$ $\emptyset$}    \\
\multicolumn{1}{|l|}{b d  $\vert$ 1 $\vert$ $\emptyset$}    \\ 
\multicolumn{1}{|l|}{c d  $\vert$ 1 $\vert$ $\emptyset$}    \\
\multicolumn{1}{|l|}{d b  $\vert$ 1 $\vert$ $\emptyset$}    \\  
\multicolumn{1}{|l|}{a d  $\vert$ 1 $\vert$ $\{bd\}$}    \\\cline{1-1}
\end{tabular}
\end{table}

As an update rule, we will multiply multiplicity for $Txy \coloneq Exz, Tzy$ joins. For $Tbd$, update: \newline  

\begin{table}[H]
\centering
\begin{tabular}{lllll}
\multicolumn{1}{|l|}{b d  $\vert$ 2 $\vert$ $\{cd\}$}    \\\cline{1-1}
\end{tabular}
\end{table}


Then:\newline

\begin{table}[H]
\centering
\begin{tabular}{lllll}
\multicolumn{1}{|l|}{a d  $\vert$ 3 $\vert$ $\{bd, cd\}$}    \\\cline{1-1}
\end{tabular}
\end{table}

Then: \newline

\begin{table}[H]
\centering
\begin{tabular}{lllll}
\multicolumn{1}{|l|}{d d  $\vert$ 2 $\vert$ $\{cd\}$}    \\\cline{1-1}
\end{tabular}
\end{table}


Then: \newline

\begin{table}[H]
\centering
\begin{tabular}{lllll}
\multicolumn{1}{|l|}{b d  $\vert$ 4 $\vert$ $\{cd, dd\}$}    \\\cline{1-1}
\end{tabular}
\end{table}


... \newline

\begin{table}[H]
\centering
\begin{tabular}{lllll}
\multicolumn{1}{|l|}{d d  $\vert$ $\infty$ $\vert$ $\{ cd, dd\}$}    \\\cline{1-1}
\end{tabular}
\end{table}


If $(d, d)$ $\rightarrow$ $\infty$, then $(b, d)$ will as well. In fact, if the fact with infinity is used as an ancestor of any other facts, the other fact will also go to infinity. In trees where the total number of elements in a tree is polynomial, one can detect infinity multiplicity in polynomial time. 

\subsection{Context Free Grammar/Push-Down Automata}

\begin{enumerate}
\item $\Sigma$ finite alphabet 
\item $\Sigma^{*}$ set of all strings/words in $\Sigma$ 
\item Formal language: $L$ ia subset of $\Sigma^{*}$
\end{enumerate} 

Hierarchy among languages (from Noam Chomsky) \newline 
\url{http://sprout038.sprout.yale.edu/imagefinder/ImageDownloadService.svc?articleid=3367694&file=rstb20120103-g2&size=LARGE} \newline 

Here, recursively enumerable is what we were using 'Turing-Decidable' as in class. 

Ex. of a non-regular language: 
\begin{enumerate}
\item $\Sigma = \{a, b\}$ 
\item $L = \{a^n b^n | n \in \mathbb{N} \}$
\end{enumerate}

There is no finite automata for this. In general, automata recognizes words, whereas grammar creates words. 

\begin{enumerate}
	\item $S \rightarrow aAb$
	\item $A \rightarrow aAb$
	\item $A \rightarrow \epsilon$
\end{enumerate}

Derivation Tree:\newline 

\begin{center}
\begin{tikzpicture}[grow'=down,scale=0.75]
\Tree [.$S$
[ . \node(n3){$b$};]
[ . \node(n1){$A$};
	[ . \node(n6){$b$};]
	[ . \node(n4){$A$};		
		[ . \node(n7){$b$};]
		[ . \node(n8){$a$};]
	]
	[ . \node(n5){$a$};]
]
[ . \node(n2){$a$};]
]
]
\end{tikzpicture}
\end{center}

Context-free grammars ($G$) lead to context-free languages ($L(G)$).\newline 

Decidable: 
\begin{enumerate}
\item if $L(G)$ is the nullset 
\item if $L(G)$ is finite/infinite
\end{enumerate}

Undecidable:
\begin{enumerate}
\item $L(G) = \Sigma^{*}$
\item $L(G_{1}) = L(G_{2})$
\end{enumerate}











