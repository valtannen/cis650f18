\section{Lecture: Thursday, October 18th}
Note taker: Yinjun
\label{sec:LC13}
\subsection{Relationships between Datalog subclasses}

The relationships between $Datalog$ and its sub-language are shown in Figure \ref{fig:datalog_subclass}, which also includes corresponding expressiveness-equivalent sub-languages in relational algebras.
\begin{figure}[H]
\begin{align*}
    (CQ \equiv SPJ) \subsetneq (UCQ \equiv SPJU \equiv non\mbox{-}recursive\ Datalog) \subset Datalog\\
    UCQ \subsetneq (non\mbox{-}recursive\ Datalog^{\neg} \equiv SPJUD(RA) \equiv domain\mbox{-}independent\ FOL)
\end{align*}
\caption{Relationship between Datalog and relational algebras languages}
\label{fig:datalog_subclass}
\end{figure}

Note that $CQ$ denotes conjunctive queries, which only includes one rule with one IDB predicate and can be expressed with SPJ (select-project-join) in relational algebra.

$UCQ$ denotes conjunctive queries with union, in which the IDB predicates only appear in the head. For a Datalog program, if there exists an IDB predicate $B$ in the body of certain rule $r$, we can simply replace $B$ with its body in $r$, which will end up with $UCQ$.

Although $UCQ$ has the same expressive power as SPJU (select-project-join-union) in relational algebra, when converting SPJU queries to $UCQ$, we need to pull out union operators from the parenthesis, which can blow up the size of the expression exponentially. It implies that evaluating $UCQ$ and $SPJU$ queries will incur different time complexity.

In terms of Datalog with negation, if there is no recursion in Datalog queries, which will be $non\mbox{-}recursive\ Datalog^{\neg}$, such class of Datalog queries is equivalent to SPJUD in expressiveness. However, unlike $UCQ$ or $CQ$, such type of queries are not monotone.

\subsubsection{Recursion detection}\label{ssec: recursive}
If there exist multiple rules in a datalog program $\mathcal{P}$, whether there exists a recursion can be detected by drawing {\em dependency graph}, which is a directed graph where each node represent one IDB predicate in $\mathcal{P}$ and there is an edge from node $X$ to $Y$ if $X$ is in the body $Y$. 

Consider the following Datalog program $\mathcal{P}$:
\begin{align*}
Txy &\coloneq Exy & (1) \\
Txy &\coloneq Exz, Tzy & (2) \\
Rz &\coloneq Txz & (3)\\
\end{align*}

Its dependency graph is presented as Figure \ref{fig:dependency_graph_cycle}, which shows that there is a cycle from node $T$ to itself. It indicates that there exists an recursion in $\mathcal{P}$.


\usetikzlibrary{arrows}

\tikzset{
    vertex/.style={circle,draw,minimum size=1.5em},
    edge/.style={->,> = latex'}
}

\begin{figure}[H]
\begin{center}
\begin{tikzpicture}[grow'=up,scale=0.75]
\tikzset{vertex/.style = {shape=circle,draw,minimum size=1.5em}}
\tikzset{edge/.style = {->,> = latex'}}
% vertices
\node[vertex] (1) at (0,0) {$T$};
\node[vertex] (2) at (2,0) {$R$};
%edges
\draw[edge] (1) to (2);
\draw [->] (-0.4,0.3) arc (10:330:20pt);
\end{tikzpicture}
\end{center}
\caption{Dependency graph of Datalog program $\mathcal{P}$}
\label{fig:dependency_graph_cycle}
\end{figure}

\subsubsection{Relationship between $CQ$ and $UCQ$}
It is trivial to show that $CQ \subset UCQ$, then we need to consider whether $CQ$ is a proper subset of $UCQ$, which can be formalized as the following theorem:

%%%proposition
\begin{theorem}
$\exists$ a $\mathcal{Q} \in UCQ$ such that $\neg \exists \mathcal{Q}' \in CQ$ such that $\forall I$\ $\mathcal{Q}(I) = \mathcal{Q}'(I)$, which is logically the same as $\exists$ a $\mathcal{Q} \in UCQ$ such that $\forall \mathcal{Q}' \in CQ$ such that $\exists I$\ $\mathcal{Q}(I) \neq \mathcal{Q}'(I)$.
\end{theorem}

We can use an example to refute the negation of the theorem above. Consider a Datalog program $\mathcal{P}$ as follow, which belongs to $UCQ$:
\begin{align*}
Qx &\coloneq Ex & (1) \\
Qx &\coloneq Fx & (2) 
\end{align*}

The output of this Datalog program is from two different sources (relations) in this program. However, for conjunctive queries, the output can be only from one source. So there should not exist a conjunctive query which can have the same output with $\mathcal{P}$.

\subsection{Finite model theory}
In order to show the expressiveness relationship between different Datalog sub-classes, there are two strategies available, i.e. {\em Ehrenfeucht-Fraïssé game} and {\em complexity theory}.

\subsubsection{Ehrenfeucht-Fraïssé game}
In Ehrenfeucht-Fraïssé game, there are two players, one spoiler and one duplicator. The spoiler tries to prove two models are equivalent while the duplicator tries to refute it, which is played in turn and round in this game. 

\subsubsection{Complexity theory}

\textbf{Time complexity on evaluating a program}
In complexity theory, different complexity classes are ordered as follows:

$AC^0\subsetneq NC^1\subset L \subset NL \subset NC^2 \subset NC \subset P \subset NP \subset \dots$

The complexity class $L$ and $NL$ represent a class of problem which can be solved with log space of tape in Turing machine deterministically and non-deterministically respectively. Typical problem in $L$ is depth-first-search problem (DFS), in which we simply need to use a work-tape (log-space) to store the index of the node in the graph. In $NL$, one example would be reachability problem in graph, which is actually a $NL$-$complete$ problem.

$AC$ is a class of complexity class modeled with circuits. $AC^0$ indicates that the circuits has depth $O(1)$ but can have unlimited-fanin AND gates and OR gates. For (domain-independent) FOL Datalog queries, they can be evaluated with the time complexity of $AC^0$.

$NC$ (called Nick's class) is another class of complexity class which models parallelism computation. $NC^i$ denotes a complexity class in which the problem can be solved within time $(log^i)$ with parallel computation on polynomial number of computers. 

All the Datalog programs can be evaluated with $P$ time. Although lots of Datalog programs can be reduced to reachability problem, which is a $NL$-$complete$ problem, it is not true for all the Datalog programs. For example, consider the following Datalog program:
\begin{align*}
Tx &\coloneq Ax & (1) \\
Tx &\coloneq Rxyz, Ty, Tz & (2) 
\end{align*}

This program cannot be reduced to reachability problem, which is thus harder than $NL$. In fact, evaluating Datalog program is $p$-$complete$, which indicates that Datalog program is not efficiently parallelizable. Recall that FOL Datalog programs can be evaluated within $AC$ time and $AC^0 \subsetneq NC^1 \subset P$. So $UCQ \subsetneq Datalog$

\textbf{Extending to negation}
We know that $Datalog \subset Datalog^{\neg}$, the complexity of $Datalog^{\neg}$ should be at least $P$. However, it should depend on semantics on negation. 

\textbf{Time complexity on determining the equivalence of two programs}
Let's go back to Figure \ref{fig:datalog_subclass}. It has been proven that determining the equivalence of two $CQ$ programs or two $UCQ$ programs is $NP$-$complete$. Besides, it is a undecidable problem to determine the equivalence of two $Datalog$ programs or two $FOL$ programs or two $Datalog^{\neg}$ programs. However, note that even though $UCQ$ and $SPJU$ have the same expressive powers, to determine the equivalence of two $SPJU$ queries, the time complexity is $\pi_{P}^2$-$complete$, which is different from $UCQ$. The reason is that when converting to equivalent $UCQ$ queries from $SPJU$ queries, the expression can become exponentially large.

In practice, determining the equivalence of two $CQ$ queries has been applied in Query rewriting using views problems, which is $NP$-$complete$ in the size of query instead of the size of database. Considering the fact that the size of queries is usually small, the overhead is acceptable in the application.