\section{Lecture: Tuesday, October 9th}
Note taker: Hui Lyu
\label{sec:LC10}

\subsection{Review}
QSQ (Query-Subquery) is a goal-directed, top-down evaluation framework with recursive sub queries. Likewise, we introduce \emph{Magicsets} which is a goal-directed, bottom-up evaluation framework.

\subsection{Magicsets}
\textbf{Magicsets} rewrites the original program to an equivalent program, avoiding computation of tuples that do not contribute to final results. It is a bottom-up evaluation framework. 

Magicsets has some features (refer to an \href{https://iccl.inf.tu-dresden.de/w/images/c/cc/DBT2016-Lecture-12.pdf}{\underline{online note}}):

\begin{itemize}
\item ``Simulate'' QSQ by Datalog rules, instead of relational algebra
\item Apply bottom-up evaluation (e.g. semi-naive evaluation on rewritten rules)
\item ``Magic sets'' are the sets of tuples stored in the auxiliary relations
\end{itemize}

\subsection{Example}
The example input and program are the same as those in Section~\ref{sec:LC9} with Figure~\ref{fig:FUD}.

So we have the following input tuples (EDB):

\begin{table}[H]
\centering
\begin{tabular}{lllll}
\multicolumn{1}{c}{\textbf{F}} & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{\textbf{U}} & \multicolumn{1}{c}{}  & \multicolumn{1}{c}{\textbf{D}} \\ \cline{1-1} \cline{3-3} \cline{5-5} 
\multicolumn{1}{|l|}{m n}      & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{f m}       & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{m f}       \\ \cline{1-1} \cline{3-3} \cline{5-5} 
\multicolumn{1}{|l|}{m o}      & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{g n}       & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{i d}       \\ \cline{1-1} \cline{3-3} \cline{5-5} 
\multicolumn{1}{|l|}{g f}      & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{i o}       & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{g b}       \\ \cline{1-1} \cline{3-3} \cline{5-5} 
                               & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{a f}       &                       &                                \\ \cline{3-3}
\end{tabular}
\end{table}

Consider the following Datalog program where $R$ is the IDB predicate:
\begin{figure}[H]
\begin{align*}
Rxy &\coloneq Fxy & (1) \\
Rxy &\coloneq Uxx_1, Ry_1x_1, Dy_1y & (2) \\
\end{align*}
\vspace{-5mm}
\caption{Original Datalog Program}
\label{fig:original-program}
\end{figure}

We want to query:
\begin{align*}
query(v) &\coloneq Rav
\end{align*}

For Magicsets, we want to:
\begin{itemize}
\item reduce number of tuples generated
\item create new relations
\end{itemize}


\subsubsection{Adornments}
Each rule in the original Datalog program can have two types of adornments with different free and bound variables in the rule head. The adorned rules are as follows (only add adornments to IDB predicate $R$):
\begin{figure}[H]
\begin{align*}
R^{bf}xy &\coloneq Fxy & (1) \\
R^{fb}xy &\coloneq Fxy & (2) \\
R^{bf}xy &\coloneq Uxx_1, R^{fb}y_1x_1, Dy_1y & (3)\\
R^{fb}xy &\coloneq Dy_1y, R^{bf}y_1x_1, Uxx_1 & (4)\\
query(v) &\coloneq R^{bf}av & (5)
\end{align*}
\vspace{-5mm}
\caption{Initially Adorned Datalog Program}
\label{fig:initial-adorn}
\end{figure}

\subsubsection{Auxiliary Relations}
Then, Magicsets framework will add some auxiliary relations to further break apart the adorned rules. The notation is $iR^{bf}a$, where $i$ denotes input. These relations are called \emph{seed} for the program. Constants (in this case $a$) in rule bodies must lead to bindings in the subquery.

Besides, we introduce supplementary predicates $S^i_j$ which means the $j$-th predicate of the $i$-th rule in the initial rewritten program in Figure~\ref{fig:initial-adorn}. Supplementary relations can be cached in between queries.

The final rewritten program is as follows:
\begin{figure}[H]
\begin{align*}
R^{bf}xy &\coloneq iR^{bf}x, Fxy & (1) \\
R^{fb}xy &\coloneq iR^{fb}y, Fxy & (2) \\
S^3_1xx_1 &\coloneq iR^{bf}x, Uxx_1 & (3.1)\\
S^3_2xy_1 &\coloneq S^3_1xx_1, R^{fb}y_1x_1 & (3.2)\\
R^{bf}xy &\coloneq S^3_2xy_1, Dy_1y & (3.3)\\
S^4_1yy_1 &\coloneq iR^{fb}y, Dy_1y & (4.1)\\
S^4_2yx_1 &\coloneq S^4_1yy_1, R^{bf}y_1x_1 & (4.2)\\
R^{fb}xy &\coloneq S^4_2yx_1, Uxx_1 & (4.3)\\
query(v) &\coloneq R^{bf}av & (5) \\
\textbf{seed}: \\
iR^{bf}a &\coloneq \\
iR^{bf}x_1 &\coloneq S^3_1 xx_1 \\
iR^{fb}y_1 &\coloneq S^4_1 yy_1
\end{align*}
\vspace{-5mm}
\caption{Adorned Datalog Program using Magicsets}
\label{fig:adorn-Magicsets}
\end{figure}

(3.1), (3.2), (3.3) are rewritten rules for rule (3) in Figure~\ref{fig:initial-adorn}. (4.1), (4.2), (4.3) are rewritten rules for rule (4) in Figure~\ref{fig:initial-adorn}. The seed in the program is added to the next recursion with more binding information. A semi-naive bottom-up evaluation can be executed in the rewritten rules.

\subsubsection{Comparison with QSQ}
Instead of using relational algebra in QSQ, Magicsets framework uses Datalog rules. The computational results are the same in this two frameworks. Both QSQ and Magicsets frameworks depend on the ordering of atoms, so their performances may or may not be better than the original semi-naive evaluation.